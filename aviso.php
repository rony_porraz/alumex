<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="es">
  <head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <link type="text/css" rel="shortcut icon" href="/img/favicon.png"/>
    <link type="image/x-icon" rel="icon" href="/img/favicon.png"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <meta name="author" content="Luminosos y Letreros Nacionales SA. de CV."/>
    <meta name="Owner" content="ventas@anunciosalumex.com">
    <meta name="Robots" content="INDEX,FOLLOW">
    <meta name="description" content="letreros luminosos, marquesinas luminosas, anuncios espectaculares, anuncios unipolares, anuncios totems, totems luminosos, pilones, pylons, signs mexico, fabricantes de anuncios, fabricantes de unipolares, fabricantes de espectaculares, fabricantes de totems, fabricantes de letreros, fabricantes de anuncios luminosos, letreros corporativos, toldos retráctiles, toldos retráctiles luminosos, gabinetes luminosos, gabinetes de luz, gabinetes 3d, cajas de luz, cajas de luz 3d, caja de luz acrílico, caja de luz lona, caja de luz led, anuncios de leds, leds de colores, fachadas en leds, iluminación de fachadas" />
    <meta name="Abstract" content="letreros luminosos, marquesinas luminosas, anuncios espectaculares, anuncios unipolares, anuncios totems, totems luminosos, pilones, pylons, signs mexico, fabricantes de anuncios">
    <meta name="Keywords" content="anuncios luminosos, letreros luminosos, letreros de aluminio, letras 3d, anuncios de acrilico, letreros para empresas, cajas de luz, totems publicitarios, unipolares, fabricacion de espectaculares, anuncios para empresas, anuncios vintage, letreros vintage ">
    <title>Fabricamos Anuncios Luminosos, Letreros Luminosos y Totems Espectaculares Para Todo México</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <link type="text/css" rel="stylesheet" media="all" href="/style/style_base.css" />
    <link type="text/css" rel="stylesheet" media="all" href="/js/led_banero/jquerysctipttop.css" />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
    <script type="text/javascript" src="/js/led_banero/jquery.leddisplay.js"></script>
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-165020589-1"></script>
    <script>
      window.dataLayer = window.dataLayer || [];
      function gtag(){dataLayer.push(arguments);}
      gtag('js', new Date());
      gtag('config', 'UA-165020589-1');
    </script>
  </head>
  <body>
    <main role="main" class="container">
      <!--Head_Baner/On-->
      <div class="container dv_head">
        <img src="/img/logo_alu.png" alt="Alumex" title="Alumex" class="img-responsive logo_des"/>
        <img src="/img/img_bagheader.png" alt="Alumex" title="Alumex" class="img-responsive ban_head  "/>
      </div>
      <!--Head_Baner/Off-->
      <!--Menu/On-->
      <?php 
          require_once('./components/menu.php');
      ?>
      <!--Menu/Off-->
      <!--Body_content/On-->
      <div class="container">
      <div class="col-md-12 conte_base">
      

      <div class="row">
                  <div class="col-lg-12">
                      <h4 class="page-header">AVISO DE PRIVACIDAD</h4>
                  </div>
              </div>





      <p>El presente documento constituye el “Aviso de Privacidad” de Luminosos y Letreros Nacionales, S.A., DE C.V., en donde se establece la manera en que será tratada su información por parte de Luminosos y Letreros Nacionales, S.A., DE C.V., así como la finalidad para la que fue recabada, lo anterior de conformidad con los Artículos 15 y 16 de Ley Federal de Protección de Datos Personales en Posesión de los Particulares (la “Ley”).</p>

      <p>Luminosos y Letreros Nacionales, S.A., DE C.V.,  respeta su derecho a la privacidad y protección de datos personales, los cuales están amparados bajo la Ley. La base de datos en donde consta su información es responsabilidad de Luminosos y Letreros Nacionales, S.A., DE C.V., la cual se encuentra ubicada en el domicilio de Amado Nervo 63, Colonia Santa María la Ribera CP 06400, Delegación Cuahutémoc, México, D. F., asimismo su información se encuentra debidamente resguardada conforme a las disposiciones de seguridad administrativa, técnica y física, establecidas en la Ley de la materia, para protegerla de los posibles daños, perdidas, alteración o acceso no autorizado.</p>

      <p><strong>Datos Sensibles</strong></p>

      <p>El Titular de la Información reconoce y acepta, que debido a su relación con Luminosos y Letreros Nacionales, S.A., DE C.V., no ha proporcionado, ni tendrá que proporcionar “datos personales sensibles”, es decir, aquellos datos personales íntimos o cuya realización debida o indebida pueda dar origen a discriminación o conlleve un riesgo grave para éste. En el supuesto de que el Titular de la Información proporcione datos del tipo de los llamados sensibles, deberá estar de acuerdo en proporcionarlos previamente y dejará a Luminosos y Letreros Nacionales, S.A., DE C.V., libre de cualquier queja o reclamación respectiva.</p>

      <p><strong>Solicitudes</strong></p>

      <p>El titular podrá ejercer los derechos de acceso, rectificación, cancelación u oposición, respecto a los datos personales que le conciernen, así como también solicitar la revocación de su consentimiento, enviando una solicitud a Luminosos y Letreros Nacionales, S.A., DE C.V., a través del teléfono 52080808 o ingresando al sitio web www.alu-mex/privacidad en donde de igual manera se le dará a conocer el procedimiento para la atención de las solicitudes de revocación del consentimiento, indicando por lo menos su nombre y domicilio completo o cualquier otro dato o documento que permita su identificación, así como el objeto de su solicitud y/o tramite a efectuar. Lo anterior se deberá realizar en base a la Ley y su Reglamento.</p>

      <p><strong>Finalidad</strong></p>

      <p>Sus datos personales e historial como cliente será utilizado por Luminosos y Letreros Nacionales, S.A., DE C.V., y quedarán registrados en nuestra base de datos, siempre que se cumpla con lo establecido en la Ley y su Reglamento respecto a cada tipo de información. Esta información puede ser utilizada por Luminosos y Letreros Nacionales, S.A., DE C.V.,  para cualquiera de las siguientes finalidades:</p>


      <p>a. Finalidad Comercial</p>

      <p>(i) Nombre del Titular de la Información, domicilio, RFC, teléfono, número de cuenta de facturación, datos bancarios.</p>

      <p>Esta información podrá usarse de manera enunciativa más no limitativa para: (1) Ofrecerle productos y servicios, ya sea de manera física, telefónica, electrónica o por cualquier otra tecnología o medio que esté al alcance de Luminosos y Letreros Nacionales, S.A., DE C.V., (2) realizar cualquier tipo de gestiones de cobranza sobre saldos pendientes de pago derivados de la relación comercial, (3) emitir órdenes de compra o solicitudes de trabajo, según corresponda y/o (4) cualquier otra acción que sea necesaria para cumplir con los intereses de Luminosos y Letreros Nacionales, S.A., DE C.V.,  respecto al acuerdo que haya llegado con el Titular de la Información.</p>

      <p>b. Finalidad laboral</p>

      <p>(i) En relación con ofertas o solicitudes de trabajo mediante la presentación de curriculum vitae, se podrá pedir información como nombre, domicilio, teléfono de contacto, correo electrónico, nivel de estudios, referencias personales, experiencia profesional, RFC, número de seguridad social, CURP, identificación oficial.</p>

      <p>Esta información podrá usarse de manera enunciativa más no limitativa para: (1) Evaluación como posible candidato para ocupar algún puesto vacante, (2) En su caso la elaboración del Contrato de Prestación de Servicios correspondiente, (3) Para proporcionar referencias en caso de que otra persona o empresa las solicite sobre candidatos.</p>

      <p>c. Finalidad de seguridad en el acceso a las instalaciones.</p>

      <p>Esta información podrá usarse de manera enunciativa más no limitativa para: (1) respaldar, registrar y/o controlar el registro de las personas que accedan o visiten las instalaciones de Luminosos y Letreros Nacionales, S.A., DE C.V., (2) cumplir con los lineamientos de seguridad establecidos en las políticas internas de la Empresa. En relación con la información que se solicite para ingresos a instalaciones de Luminosos y Letreros Nacionales, S.A., DE C.V., se podrá pedir información como nombre completo, firma, identificación oficial, así como información de equipo electrónico que se vaya a ingresar a las instalaciones de Luminosos y Letreros Nacionales, S.A., DE C.V..</p>

      <p><strong>Transferencia</strong></p>

      <p>El Titular de la Información entiende y acepta que Luminosos y Letreros Nacionales, S.A., DE C.V., podrá transferir sus datos personales a terceros que han sido contratados por Luminosos y Letreros Nacionales, S.A., DE C.V.,  para que realicen en su nombre y representación ciertas tareas relacionadas con las actividades comerciales y de promoción de sus productos y/o servicios. Estas terceras partes pueden tratar los datos en cumplimiento de las instrucciones de Luminosos y Letreros Nacionales, S.A., DE C.V., o tomar decisiones sobre ellos como parte de la prestación de sus servicios. En cualquiera de los dos casos Luminosos y Letreros Nacionales, S.A., DE C.V.,  seleccionará proveedores que considere confiables y que se comprometan, mediante un contrato u otros medios legales aplicables, a implementar las medidas de seguridad necesarias para garantizar un nivel de protección adecuado a sus datos personales. Derivado de lo anterior Luminosos y Letreros Nacionales, S.A., DE C.V., exigirá a sus proveedores que cumplan con medidas de seguridad que garanticen los mismos niveles de protección que Luminosos y Letreros Nacionales, S.A., DE C.V.,  implementa durante el tratamiento de sus datos como cliente de Luminosos y Letreros Nacionales, S.A., DE C.V.. Estas terceras partes seleccionadas tendrán acceso a su información con la finalidad de realizar las tareas especificadas en el contrato de servicios aplicable que haya suscrito con Luminosos y Letreros Nacionales, S.A., DE C.V.. Si  Luminosos y Letreros Nacionales, S.A., DE C.V., determina que un proveedor no está cumpliendo con las obligaciones pactadas, tomará inmediatamente las acciones pertinentes.</p>

      <p>Si el titular, ya no acepta la transmisión de sus datos personales de conformidad con lo estipulado en el párrafo anterior, puede ponerse en contacto con Luminosos y Letreros Nacionales, S.A., DE C.V., por cualquiera de los medios establecidos en el presente Aviso de Privacidad.</p>

      <p><strong>Excepciones</strong></p>

      <p>Adicionalmente y de conformidad con lo estipulado en los Artículos 10, 37 y demás relativos de la Ley y su Reglamento, Luminosos y Letreros Nacionales, S.A., DE C.V.,  quedara exceptuado de las obligaciones referentes al consentimiento para el Tratamiento y Transferencia de sus Datos, cuando:</p>

      <p>I. Esté previsto en una Ley;</p>

      <p>II. Los datos figuren en fuentes de acceso público;</p>

      <p>III. Los datos personales se sometan a un procedimiento previo de disociación;</p>

      <p>IV. Tenga el propósito de cumplir obligaciones derivadas de una relación jurídica entre el titular y el responsable;</p>

      <p>V. Exista una situación de emergencia que potencialmente pueda dañar a un individuo en su persona o en sus bienes;</p>

      <p>VI. Sean indispensables para la atención médica, la prevención, diagnóstico, la prestación de asistencia sanitaria, tratamientos médicos o la gestión de servicios sanitarios;</p>

      <p>VII. Se dicte resolución de autoridad competente;</p>

      <p>VIII. Cuando la transferencia sea precisa para el reconocimiento, ejercicio o defensa de un derecho en un proceso judicial </p>

      <p>IX. Cuando la transferencia sea precisa para el mantenimiento o cumplimiento de una relación jurídica entre el responsable y el titular.</p>

      <p><strong>Modificaciones</strong></p>

      <p>Ambas partes, en este acto acuerdan, que en caso de que se requiera alguna modificación a lo estipulado en el presente Aviso de Privacidad, Luminosos y Letreros Nacionales, S.A., DE C.V.,  se obliga a hacer del conocimiento los cambios que en su caso se requieran, por cualquier medio, incluidos los electrónicos, previo aviso que se le de a Usted para que se manifieste por su parte, lo que a su derecho convenga, ya que de no recibir negativa expresa y por escrito de su parte, o bien, respuesta alguna, se entenderá que Usted acepta de conformidad los cambios realizados.</p>

      <p><strong>Consentimiento del Titular</strong></p>

      <p>El Titular de la Información reconoce y acepta que en caso de que este “Aviso de Privacidad” esté disponible a través de una página electrónica (sitio Web, página de Internet o similar) o algún otro dispositivo electrónico, al hacer clic en “aceptar” o de cualquier otra forma seguir navegando en el sitio, o bien al proporcionar sus Datos a través del mismo o a través de cualquier medio electrónico (correo electrónico, etc), constituye una manifestación de su consentimiento para que Luminosos y Letreros Nacionales, S.A., DE C.V., realice el tratamiento de sus Datos, de conformidad con este Aviso de Privacidad.</p>

      <p>Asimismo de igual manera manifiesta que en caso de que este “Aviso de Privacidad” esté disponible por escrito, su firma, rúbrica, nombre o huella o bien al proporcionar sus Datos, constituye una manifestación de su consentimiento para que Luminosos y Letreros Nacionales, S.A., DE C.V realice el tratamiento de sus datos, de conformidad con este “Aviso de Privacidad”.</p>




      
      </div>
      </div>
      <!--Body_content/Off-->

      <!--Footer/On-->
      <?php 
            require_once('./components/footer.php');
      ?>
      <!--Footer/Off-->
      
    </main>

    <?php 
        require_once('./components/navfloat.php');
    ?>
    
</body>
</html>