$(document).ready(function() {
    $('#carouselControls').carousel({ interval: 500});
    $('#carouselControlsindex').carousel({ interval: 500});
    $('#carouselControlsindex2').carousel({ interval: 500});
    $('#carouselControlsindex3').carousel({ interval: 500});
    $('#carouselControlsindex4').carousel({ interval: 500});
    var $cont = $('#txt_cont').val(); //total de imagenes
    $("#btn_mas").click(function(){
      $("#btn_mas").addClass("hidden");
      $("#btn_menos").removeClass("hidden");
      $('[id^=imgOculto]').removeClass("hidden");
    });
      
    $("#btn_menos").click(function(){
      $("#btn_mas").removeClass("hidden");
      $("#btn_menos").addClass("hidden");
      $('[id^=imgOculto]').addClass("hidden");
    });
  });
