document.addEventListener('DOMContentLoaded' , () => {

    let stateButton = false

    const btn_mas = document.querySelector('#btn_mas')
    
    btn_mas.addEventListener('click' , () => {

        const wrapper = document.querySelector('#gallery')
        const imagesToSee = document.querySelectorAll('#gallery img.hidden')
        stateButton = !stateButton

        switch( stateButton )
        {
            case true:

                btn_mas.classList.remove('fa-plus-circle')
                btn_mas.classList.add('fa-minus-circle')
                imagesToSee.forEach( img => {
                    img.classList.remove('hidden')
                    img.classList.add('toHidden')
                })

            break
            case false:

                const btnAOcultar = document.querySelectorAll('#gallery img.toHidden')

                btnAOcultar.forEach( img => {
                    img.classList.remove('toHidden')
                    img.classList.add('hidden')
                })

                btn_mas.classList.remove('fa-minus-circle')
                btn_mas.classList.add('fa-plus-circle')
                window.scrollTo({ top: 0 })

            break
        }

        //console.log(wrapper);
        //console.log(imagesToSee);

        


    })

})