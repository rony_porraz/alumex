<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="es">
  <head>
    <?php 
        require './components/config.php';
    ?>
    <title>Letreros Vintage - <?php echo $subTitle;?></title>
    <link rel="stylesheet" href="style/boot.css">
    <link type="text/css" rel="stylesheet" media="all" href="style/style_base.css" />
    <link type="text/css" rel="stylesheet" media="all" href="/js/led_banero/jquerysctipttop.css" />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
    <script src="js/jquery.js"></script>
    <script src="js/popper.js"></script>
    <script src="js/boot.js"></script>
    <script type="text/javascript" src="/js/led_banero/jquery.leddisplay.js"></script>
    <script src="js/boton-mas.js"></script>
  </head>
  <body>
    <main role="main" class="container">
      <!--Head_Baner/On-->
      <div class="container dv_head">
        <img loading="lazy" src="/img/logo_alu.png" alt="Alumex" title="Alumex" class="img-fluid logo_des"/>
        <img loading="lazy" src="/img/img_bagheader.png" alt="Alumex" title="Alumex" class="img-fluid ban_head  "/>
      </div>
      <!--Head_Baner/Off-->
      <!--Menu/On-->
        <?php 
            require_once('./components/menu.php');
        ?>
      <!--Menu/Off-->
      <!--Body_content/On-->
      <div class="container">
        <div class="col-md-12 conte_base">
          <!--On/Title-->
          <div class="row">
            <div class="col-lg-12">
                <h1 style="font-size: 1.5rem; margin: 10px 0;" class="page-header">Letreros Vintage</h1>
            </div>
          </div>
          <!--Off/Tile-->
          <!--On/Galery-->
          <div class="row custom" id="gallery" data-toggle="modal" data-target="#exampleModal">
            <div class="col-12 col-sm-6 col-lg-4 img-fluid"><img loading="lazy" class="w-100 img-fluid" data-target="#carouselExample" data-slide-to="0" alt="2.3_ANUNCIO-LUMINOSO-CAMALEON-VINTAGE-2-FLECHA-INSTALACION.jpg - Alu-Mex" title="2.3_ANUNCIO-LUMINOSO-CAMALEON-VINTAGE-2-FLECHA-INSTALACION.jpg - Alu-Mex"  src="/img/Letreros_Vintage/2.3_ANUNCIO-LUMINOSO-CAMALEON-VINTAGE-2-FLECHA-INSTALACION.jpg"></div>
            <div class="col-12 col-sm-6 col-lg-4 img-fluid"><img loading="lazy" class="w-100 img-fluid" data-target="#carouselExample" data-slide-to="1" alt="ANUNCIO-LUMINOSO-CAMALEON-LUMINOSO-LETRERO-VINTAGE - Alu-Mex" title="ANUNCIO-LUMINOSO-CAMALEON-LUMINOSO-LETRERO-VINTAGE - Alu-Mex"  src="/img/Letreros_Vintage/ANUNCIO-LUMINOSO-CAMALEON-LUMINOSO-LETRERO-VINTAGE.webp"></div>
            <div class="col-12 col-sm-6 col-lg-4 img-fluid"><img loading="lazy" class="w-100 img-fluid" data-target="#carouselExample" data-slide-to="2" alt="ANUNCIO-LUMINOSO-DEBARBAS-VINTAGE-FLECHA-3D-LETRERO-LUMINOSO - Alu-Mex" title="ANUNCIO-LUMINOSO-DEBARBAS-VINTAGE-FLECHA-3D-LETRERO-LUMINOSO - Alu-Mex"  src="/img/Letreros_Vintage/ANUNCIO-LUMINOSO-DEBARBAS-VINTAGE-FLECHA-3D-LETRERO-LUMINOSO.webp"></div>
            <div class="col-12 col-sm-6 col-lg-4 img-fluid"><img loading="lazy" class="w-100 img-fluid" data-target="#carouselExample" data-slide-to="3" alt="ANUNCIOS-LUMINOSOS-CAFE-GARAT-FACHADA-AMARILLO-CAFE-3 - Alu-Mex" title="ANUNCIOS-LUMINOSOS-CAFE-GARAT-FACHADA-AMARILLO-CAFE-3 - Alu-Mex"  src="/img/Letreros_Vintage/ANUNCIOS-LUMINOSOS-CAFE-GARAT-FACHADA-AMARILLO-CAFE-3.webp"></div>
            <div class="col-12 col-sm-6 col-lg-4 img-fluid"><img loading="lazy" class="w-100 img-fluid" data-target="#carouselExample" data-slide-to="4" alt="ANUNCIOS-LUMINOSOS-HOTEL-COHIBA-VINTAGE-FACHADA - Alu-Mex" title="ANUNCIOS-LUMINOSOS-HOTEL-COHIBA-VINTAGE-FACHADA - Alu-Mex"  src="/img/Letreros_Vintage/ANUNCIOS-LUMINOSOS-HOTEL-COHIBA-VINTAGE-FACHADA.webp"></div>
            <div class="col-12 col-sm-6 col-lg-4 img-fluid"><img loading="lazy" class="w-100 img-fluid" data-target="#carouselExample" data-slide-to="5" alt="BOTON-LUMINOSO-TEIKIT-VINTAGE-FLECHA-ROJO-2 - Alu-Mex" title="BOTON-LUMINOSO-TEIKIT-VINTAGE-FLECHA-ROJO-2 - Alu-Mex"  src="/img/Letreros_Vintage/BOTON-LUMINOSO-TEIKIT-VINTAGE-FLECHA-ROJO-2.webp"></div>
            <div class="col-12 col-sm-6 col-lg-4 img-fluid"><img loading="lazy" class="w-100 img-fluid" data-target="#carouselExample" data-slide-to="6" alt="BOTON-LUMINOSO-TEIKIT-VINTAGE-FLECHA-ROJO - Alu-Mex" title="BOTON-LUMINOSO-TEIKIT-VINTAGE-FLECHA-ROJO - Alu-Mex"  src="/img/Letreros_Vintage/BOTON-LUMINOSO-TEIKIT-VINTAGE-FLECHA-ROJO.webp"></div>
            <div class="col-12 col-sm-6 col-lg-4 img-fluid"><img loading="lazy" class="w-100 img-fluid " data-target="#carouselExample" data-slide-to="7" alt="LETRERO-VINTAGE-ANUNCIO-LUMINOSO-CAMALEON-VINTAGE-2-BLANCO - Alu-Mex" title="LETRERO-VINTAGE-ANUNCIO-LUMINOSO-CAMALEON-VINTAGE-2-BLANCO - Alu-Mex"  src="/img/Letreros_Vintage/LETRERO-VINTAGE-ANUNCIO-LUMINOSO-CAMALEON-VINTAGE-2-BLANCO.webp"></div>
            <div class="col-12 col-sm-6 col-lg-4 img-fluid"><img loading="lazy" class="w-100 img-fluid " data-target="#carouselExample" data-slide-to="8" alt="LETRERO-VINTAGE-ANUNCIO-LUMINOSO-CAMALEON-VINTAGE-3-COLORES - Alu-Mex" title="LETRERO-VINTAGE-ANUNCIO-LUMINOSO-CAMALEON-VINTAGE-3-COLORES - Alu-Mex"  src="/img/Letreros_Vintage/LETRERO-VINTAGE-ANUNCIO-LUMINOSO-CAMALEON-VINTAGE-3-COLORES.webp"></div>
            <div class="col-12 col-sm-6 col-lg-4 img-fluid"><img loading="lazy" class="w-100 img-fluid " data-target="#carouselExample" data-slide-to="9" alt="LETRERO-VINTAGE-ANUNCIO-LUMINOSO-LIQUOR-LAB-VINTAGE-LETRAS-ACRILICO-VERDE-BLANCO-2 - Alu-Mex" title="LETRERO-VINTAGE-ANUNCIO-LUMINOSO-LIQUOR-LAB-VINTAGE-LETRAS-ACRILICO-VERDE-BLANCO-2 - Alu-Mex"  src="/img/Letreros_Vintage/LETRERO-VINTAGE-ANUNCIO-LUMINOSO-LIQUOR-LAB-VINTAGE-LETRAS-ACRILICO-VERDE-BLANCO-2.webp"></div>
            <div class="col-12 col-sm-6 col-lg-4 img-fluid"><img loading="lazy" class="w-100 img-fluid " data-target="#carouselExample" data-slide-to="10" alt="LETRERO-VINTAGE-ANUNCIO-LUMINOSO-LIQUOR-LAB-VINTAGE-LETRAS-ACRILICO-VERDE-BLANCO-3 - Alu-Mex" title="LETRERO-VINTAGE-ANUNCIO-LUMINOSO-LIQUOR-LAB-VINTAGE-LETRAS-ACRILICO-VERDE-BLANCO-3 - Alu-Mex"  src="/img/Letreros_Vintage/LETRERO-VINTAGE-ANUNCIO-LUMINOSO-LIQUOR-LAB-VINTAGE-LETRAS-ACRILICO-VERDE-BLANCO-3.webp"></div>
            <div class="col-12 col-sm-6 col-lg-4 img-fluid"><img loading="lazy" class="w-100 img-fluid " data-target="#carouselExample" data-slide-to="11" alt="LETRERO-VINTAGE-ANUNCIO-LUMINOSO-LIQUOR-LAB-VINTAGE-LETRAS-ACRILICO-VERDE-BLANCO - Alu-Mex" title="LETRERO-VINTAGE-ANUNCIO-LUMINOSO-LIQUOR-LAB-VINTAGE-LETRAS-ACRILICO-VERDE-BLANCO - Alu-Mex"  src="/img/Letreros_Vintage/LETRERO-VINTAGE-ANUNCIO-LUMINOSO-LIQUOR-LAB-VINTAGE-LETRAS-ACRILICO-VERDE-BLANCO.webp"></div>
            <div class="col-12 col-sm-6 col-lg-4 img-fluid"><img loading="lazy" class="w-100 img-fluid hidden" data-target="#carouselExample" data-slide-to="12" alt="LETRERO-VINTAGE-BRASSI-ANUNCIO-LUMINOSO-3D-VINTAGE - Alu-Mex" title="LETRERO-VINTAGE-BRASSI-ANUNCIO-LUMINOSO-3D-VINTAGE - Alu-Mex"  src="/img/Letreros_Vintage/LETRERO-VINTAGE-BRASSI-ANUNCIO-LUMINOSO-3D-VINTAGE.webp"></div>
            <div class="col-12 col-sm-6 col-lg-4 img-fluid"><img loading="lazy" class="w-100 img-fluid hidden" data-target="#carouselExample" data-slide-to="13" alt="LETRERO-VINTAGE-CAFE-ANUNCIO-LUMINOSO-VINTAGE-3D - Alu-Mex" title="LETRERO-VINTAGE-CAFE-ANUNCIO-LUMINOSO-VINTAGE-3D - Alu-Mex"  src="/img/Letreros_Vintage/LETRERO-VINTAGE-CAFE-ANUNCIO-LUMINOSO-VINTAGE-3D.webp"></div>
            <div class="col-12 col-sm-6 col-lg-4 img-fluid"><img loading="lazy" class="w-100 img-fluid hidden" data-target="#carouselExample" data-slide-to="14" alt="LETRERO-VINTAGE-CAMALEON-2-ANUNCIO-LUMINOSO-VINTAGE-3D - Alu-Mex" title="LETRERO-VINTAGE-CAMALEON-2-ANUNCIO-LUMINOSO-VINTAGE-3D - Alu-Mex"  src="/img/Letreros_Vintage/LETRERO-VINTAGE-CAMALEON-2-ANUNCIO-LUMINOSO-VINTAGE-3D.webp"></div>
            <div class="col-12 col-sm-6 col-lg-4 img-fluid"><img loading="lazy" class="w-100 img-fluid hidden" data-target="#carouselExample" data-slide-to="15" alt="LETRERO-VINTAGE-CAMALEON-ANUNCIO-LUMINOSO-VINTAGE-3D - Alu-Mex" title="LETRERO-VINTAGE-CAMALEON-ANUNCIO-LUMINOSO-VINTAGE-3D - Alu-Mex"  src="/img/Letreros_Vintage/LETRERO-VINTAGE-CAMALEON-ANUNCIO-LUMINOSO-VINTAGE-3D.webp"></div>
            <div class="col-12 col-sm-6 col-lg-4 img-fluid"><img loading="lazy" class="w-100 img-fluid hidden" data-target="#carouselExample" data-slide-to="16" alt="LETRERO-VINTAGE-CAMALEON-LUMINOSO-LETRERO-VINTAGE - Alu-Mex" title="LETRERO-VINTAGE-CAMALEON-LUMINOSO-LETRERO-VINTAGE - Alu-Mex"  src="/img/Letreros_Vintage/LETRERO-VINTAGE-CAMALEON-LUMINOSO-LETRERO-VINTAGE.webp"></div>
            <div class="col-12 col-sm-6 col-lg-4 img-fluid"><img loading="lazy" class="w-100 img-fluid hidden" data-target="#carouselExample" data-slide-to="17" alt="LETRERO-VINTAGE-CAMALEON-VINTAGE-2-FLECHA-INSTALACION - Alu-Mex" title="LETRERO-VINTAGE-CAMALEON-VINTAGE-2-FLECHA-INSTALACION - Alu-Mex"  src="/img/Letreros_Vintage/LETRERO-VINTAGE-CAMALEON-VINTAGE-2-FLECHA-INSTALACION.webp"></div>
            <div class="col-12 col-sm-6 col-lg-4 img-fluid"><img loading="lazy" class="w-100 img-fluid hidden" data-target="#carouselExample" data-slide-to="18" alt="LETRERO-VINTAGE-CAMALEON-VINTAGE-3-FLECHA - Alu-Mex" title="LETRERO-VINTAGE-CAMALEON-VINTAGE-3-FLECHA - Alu-Mex"  src="/img/Letreros_Vintage/LETRERO-VINTAGE-CAMALEON-VINTAGE-3-FLECHA.webp"></div>
            <div class="col-12 col-sm-6 col-lg-4 img-fluid"><img loading="lazy" class="w-100 img-fluid hidden" data-target="#carouselExample" data-slide-to="19" alt="LETRERO-VINTAGE-CAMALEON-VINTAGE-4-FLECHA - Alu-Mex" title="LETRERO-VINTAGE-CAMALEON-VINTAGE-4-FLECHA - Alu-Mex"  src="/img/Letreros_Vintage/LETRERO-VINTAGE-CAMALEON-VINTAGE-4-FLECHA.webp"></div>
            <div class="col-12 col-sm-6 col-lg-4 img-fluid"><img loading="lazy" class="w-100 img-fluid hidden" data-target="#carouselExample" data-slide-to="20" alt="LETRERO-VINTAGE-CATAMARAN-ANUNCIO-LUMINOSO-VINTAGE-3D - Alu-Mex" title="LETRERO-VINTAGE-CATAMARAN-ANUNCIO-LUMINOSO-VINTAGE-3D - Alu-Mex"  src="/img/Letreros_Vintage/LETRERO-VINTAGE-CATAMARAN-ANUNCIO-LUMINOSO-VINTAGE-3D.webp"></div>
            <div class="col-12 col-sm-6 col-lg-4 img-fluid"><img loading="lazy" class="w-100 img-fluid hidden" data-target="#carouselExample" data-slide-to="21" alt="LETRERO-VINTAGE-CNK-POST-ALUMEX-ACRILICO-2 - Alu-Mex" title="LETRERO-VINTAGE-CNK-POST-ALUMEX-ACRILICO-2 - Alu-Mex"  src="/img/Letreros_Vintage/LETRERO-VINTAGE-CNK-POST-ALUMEX-ACRILICO-2.webp"></div>
            <div class="col-12 col-sm-6 col-lg-4 img-fluid"><img loading="lazy" class="w-100 img-fluid hidden" data-target="#carouselExample" data-slide-to="22" alt="LETRERO-VINTAGE-CNK-POST-ALUMEX-ACRILICO-3 - Alu-Mex" title="LETRERO-VINTAGE-CNK-POST-ALUMEX-ACRILICO-3 - Alu-Mex"  src="/img/Letreros_Vintage/LETRERO-VINTAGE-CNK-POST-ALUMEX-ACRILICO-3.webp"></div>
            <div class="col-12 col-sm-6 col-lg-4 img-fluid"><img loading="lazy" class="w-100 img-fluid hidden" data-target="#carouselExample" data-slide-to="23" alt="LETRERO-VINTAGE-COHIBA-HOTEL-LUMINOSO-ANUNCIO-3D-LUMINOSO - Alu-Mex" title="LETRERO-VINTAGE-COHIBA-HOTEL-LUMINOSO-ANUNCIO-3D-LUMINOSO - Alu-Mex"  src="/img/Letreros_Vintage/LETRERO-VINTAGE-COHIBA-HOTEL-LUMINOSO-ANUNCIO-3D-LUMINOSO.webp"></div>
            <div class="col-12 col-sm-6 col-lg-4 img-fluid"><img loading="lazy" class="w-100 img-fluid hidden" data-target="#carouselExample" data-slide-to="24" alt="LETRERO-VINTAGE-DEBARBAS-VINTAGE-FLECHA-3D-LETRERO-LUMINOSO - Alu-Mex" title="LETRERO-VINTAGE-DEBARBAS-VINTAGE-FLECHA-3D-LETRERO-LUMINOSO - Alu-Mex"  src="/img/Letreros_Vintage/LETRERO-VINTAGE-DEBARBAS-VINTAGE-FLECHA-3D-LETRERO-LUMINOSO.webp"></div>
            <div class="col-12 col-sm-6 col-lg-4 img-fluid"><img loading="lazy" class="w-100 img-fluid hidden" data-target="#carouselExample" data-slide-to="25" alt="LETRERO-VINTAGE-GRAN-CIBELES-ANUNCIO-LUMINOSO-VINTAGE-3D - Alu-Mex" title="LETRERO-VINTAGE-GRAN-CIBELES-ANUNCIO-LUMINOSO-VINTAGE-3D - Alu-Mex"  src="/img/Letreros_Vintage/LETRERO-VINTAGE-GRAN-CIBELES-ANUNCIO-LUMINOSO-VINTAGE-3D.webp"></div>
            <div class="col-12 col-sm-6 col-lg-4 img-fluid"><img loading="lazy" class="w-100 img-fluid hidden" data-target="#carouselExample" data-slide-to="26" alt="LETRERO-VINTAGE-LA-COLOMBA-NEGRO-ROJO-ACRILICO-TERMOFORMADO-2 - Alu-Mex" title="LETRERO-VINTAGE-LA-COLOMBA-NEGRO-ROJO-ACRILICO-TERMOFORMADO-2 - Alu-Mex"  src="/img/Letreros_Vintage/LETRERO-VINTAGE-LA-COLOMBA-NEGRO-ROJO-ACRILICO-TERMOFORMADO-2.webp"></div>
            <div class="col-12 col-sm-6 col-lg-4 img-fluid"><img loading="lazy" class="w-100 img-fluid hidden" data-target="#carouselExample" data-slide-to="27" alt="LETRERO-VINTAGE-LETS-HIT-THE-ROAD-3D-ANUNCIO-LUMINOSO-VINTAGE - Alu-Mex" title="LETRERO-VINTAGE-LETS-HIT-THE-ROAD-3D-ANUNCIO-LUMINOSO-VINTAGE - Alu-Mex"  src="/img/Letreros_Vintage/LETRERO-VINTAGE-LETS-HIT-THE-ROAD-3D-ANUNCIO-LUMINOSO-VINTAGE.webp"></div>
            <div class="col-12 col-sm-6 col-lg-4 img-fluid"><img loading="lazy" class="w-100 img-fluid hidden" data-target="#carouselExample" data-slide-to="28" alt="LETRERO-VINTAGE-MEXSI-ROCU-ANUNCIO-LUMINOSO-VINTAGE-3D - Alu-Mex" title="LETRERO-VINTAGE-MEXSI-ROCU-ANUNCIO-LUMINOSO-VINTAGE-3D - Alu-Mex"  src="/img/Letreros_Vintage/LETRERO-VINTAGE-MEXSI-ROCU-ANUNCIO-LUMINOSO-VINTAGE-3D.webp"></div>
            <div class="col-12 col-sm-6 col-lg-4 img-fluid"><img loading="lazy" class="w-100 img-fluid hidden" data-target="#carouselExample" data-slide-to="29" alt="LETRERO-VINTAGE-OSHKOSH-3D-ANUNCIO-LUMINOSO-VINTAGE - Alu-Mex" title="LETRERO-VINTAGE-OSHKOSH-3D-ANUNCIO-LUMINOSO-VINTAGE - Alu-Mex"  src="/img/Letreros_Vintage/LETRERO-VINTAGE-OSHKOSH-3D-ANUNCIO-LUMINOSO-VINTAGE.webp"></div>
            <div class="col-12 col-sm-6 col-lg-4 img-fluid"><img loading="lazy" class="w-100 img-fluid hidden" data-target="#carouselExample" data-slide-to="30" alt="LETRERO-VINTAGE-SUSHI-ANUNCIO-LUMINOSO-VINTAGE-3D - Alu-Mex" title="LETRERO-VINTAGE-SUSHI-ANUNCIO-LUMINOSO-VINTAGE-3D - Alu-Mex"  src="/img/Letreros_Vintage/LETRERO-VINTAGE-SUSHI-ANUNCIO-LUMINOSO-VINTAGE-3D.webp"></div>
            <div class="col-12 col-sm-6 col-lg-4 img-fluid"><img loading="lazy" class="w-100 img-fluid hidden" data-target="#carouselExample" data-slide-to="31" alt="LETRERO-VINTAGE-TEIKIT-ANUNCIO-LUMINOSO-VINTAGE-3D - Alu-Mex" title="LETRERO-VINTAGE-TEIKIT-ANUNCIO-LUMINOSO-VINTAGE-3D - Alu-Mex"  src="/img/Letreros_Vintage/LETRERO-VINTAGE-TEIKIT-ANUNCIO-LUMINOSO-VINTAGE-3D.webp"></div>
            <div class="col-12 col-sm-6 col-lg-4 img-fluid"><img loading="lazy" class="w-100 img-fluid hidden" data-target="#carouselExample" data-slide-to="32" alt="LETREROS-LUMINOSOS-DON-GALLO-ROSTICEROS-FACHADA-INSTALACION-ACERO-RAUTEADO-4 - Alu-Mex" title="LETREROS-LUMINOSOS-DON-GALLO-ROSTICEROS-FACHADA-INSTALACION-ACERO-RAUTEADO-4 - Alu-Mex"  src="/img/Letreros_Vintage/LETREROS-LUMINOSOS-DON-GALLO-ROSTICEROS-FACHADA-INSTALACION-ACERO-RAUTEADO-4.webp"></div>
            <div class="col-12 col-sm-6 col-lg-4 img-fluid"><img loading="lazy" class="w-100 img-fluid hidden" data-target="#carouselExample" data-slide-to="33" alt="LETREROS-VINTAGE-DON-GALLO-ROSTICEROS-FACHADA-INSTALACION-ACERO-RAUTEADO-5 - Alu-Mex" title="LETREROS-VINTAGE-DON-GALLO-ROSTICEROS-FACHADA-INSTALACION-ACERO-RAUTEADO-5 - Alu-Mex"  src="/img/Letreros_Vintage/LETREROS-VINTAGE-DON-GALLO-ROSTICEROS-FACHADA-INSTALACION-ACERO-RAUTEADO-5.webp"></div>
            
          </div>
          <!--On/btnplus-->
          <div class="col-md-12 text-center">
            <h1><span class="fa fa-plus-circle" title="Ver más" alt="Ver más" id="btn_mas"></span></h1>
            <h1><span class="fa fa-minus-circle hidden" title="Ver menos" alt="Ver menos" id="btn_menos"></span></h1>
          </div>
          <!--Off/btnplus-->
          <!--Off/Galery-->
          <!-- Modal -->
          <div class="modal fade custom" id="exampleModal" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog" role="document">
              <div class="modal-content">
                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                <div class="modal-body">
                  <div id="carouselExample" class="carousel slide" data-ride="carousel">
                    <div class="carousel-inner">
                      <div class="carousel-item active"><img loading="lazy" class="w-100 img-fluid" data-target="#carouselExample" data-slide-to="0" alt="2.3_ANUNCIO-LUMINOSO-CAMALEON-VINTAGE-2-FLECHA-INSTALACION.jpg - Alu-Mex" title="2.3_ANUNCIO-LUMINOSO-CAMALEON-VINTAGE-2-FLECHA-INSTALACION.jpg - Alu-Mex"  src="/img/Letreros_Vintage/2.3_ANUNCIO-LUMINOSO-CAMALEON-VINTAGE-2-FLECHA-INSTALACION.jpg"></div>
                      <div class="carousel-item"><img loading="lazy" class="w-100 img-fluid" data-target="#carouselExample" data-slide-to="1" alt="ANUNCIO-LUMINOSO-CAMALEON-LUMINOSO-LETRERO-VINTAGE - Alu-Mex" title="ANUNCIO-LUMINOSO-CAMALEON-LUMINOSO-LETRERO-VINTAGE - Alu-Mex"  src="/img/Letreros_Vintage/ANUNCIO-LUMINOSO-CAMALEON-LUMINOSO-LETRERO-VINTAGE.webp"></div>
                      <div class="carousel-item"><img loading="lazy" class="w-100 img-fluid" data-target="#carouselExample" data-slide-to="2" alt="ANUNCIO-LUMINOSO-DEBARBAS-VINTAGE-FLECHA-3D-LETRERO-LUMINOSO - Alu-Mex" title="ANUNCIO-LUMINOSO-DEBARBAS-VINTAGE-FLECHA-3D-LETRERO-LUMINOSO - Alu-Mex"  src="/img/Letreros_Vintage/ANUNCIO-LUMINOSO-DEBARBAS-VINTAGE-FLECHA-3D-LETRERO-LUMINOSO.webp"></div>
                      <div class="carousel-item"><img loading="lazy" class="w-100 img-fluid" data-target="#carouselExample" data-slide-to="3" alt="ANUNCIOS-LUMINOSOS-CAFE-GARAT-FACHADA-AMARILLO-CAFE-3 - Alu-Mex" title="ANUNCIOS-LUMINOSOS-CAFE-GARAT-FACHADA-AMARILLO-CAFE-3 - Alu-Mex"  src="/img/Letreros_Vintage/ANUNCIOS-LUMINOSOS-CAFE-GARAT-FACHADA-AMARILLO-CAFE-3.webp"></div>
                      <div class="carousel-item"><img loading="lazy" class="w-100 img-fluid" data-target="#carouselExample" data-slide-to="4" alt="ANUNCIOS-LUMINOSOS-HOTEL-COHIBA-VINTAGE-FACHADA - Alu-Mex" title="ANUNCIOS-LUMINOSOS-HOTEL-COHIBA-VINTAGE-FACHADA - Alu-Mex"  src="/img/Letreros_Vintage/ANUNCIOS-LUMINOSOS-HOTEL-COHIBA-VINTAGE-FACHADA.webp"></div>
                      <div class="carousel-item"><img loading="lazy" class="w-100 img-fluid" data-target="#carouselExample" data-slide-to="5" alt="BOTON-LUMINOSO-TEIKIT-VINTAGE-FLECHA-ROJO-2 - Alu-Mex" title="BOTON-LUMINOSO-TEIKIT-VINTAGE-FLECHA-ROJO-2 - Alu-Mex"  src="/img/Letreros_Vintage/BOTON-LUMINOSO-TEIKIT-VINTAGE-FLECHA-ROJO-2.webp"></div>
                      <div class="carousel-item"><img loading="lazy" class="w-100 img-fluid" data-target="#carouselExample" data-slide-to="6" alt="BOTON-LUMINOSO-TEIKIT-VINTAGE-FLECHA-ROJO - Alu-Mex" title="BOTON-LUMINOSO-TEIKIT-VINTAGE-FLECHA-ROJO - Alu-Mex"  src="/img/Letreros_Vintage/BOTON-LUMINOSO-TEIKIT-VINTAGE-FLECHA-ROJO.webp"></div>
                      <div class="carousel-item"><img loading="lazy" class="w-100 img-fluid" data-target="#carouselExample" data-slide-to="7" alt="LETRERO-VINTAGE-ANUNCIO-LUMINOSO-CAMALEON-VINTAGE-2-BLANCO - Alu-Mex" title="LETRERO-VINTAGE-ANUNCIO-LUMINOSO-CAMALEON-VINTAGE-2-BLANCO - Alu-Mex"  src="/img/Letreros_Vintage/LETRERO-VINTAGE-ANUNCIO-LUMINOSO-CAMALEON-VINTAGE-2-BLANCO.webp"></div>
                      <div class="carousel-item"><img loading="lazy" class="w-100 img-fluid" data-target="#carouselExample" data-slide-to="8" alt="LETRERO-VINTAGE-ANUNCIO-LUMINOSO-CAMALEON-VINTAGE-3-COLORES - Alu-Mex" title="LETRERO-VINTAGE-ANUNCIO-LUMINOSO-CAMALEON-VINTAGE-3-COLORES - Alu-Mex"  src="/img/Letreros_Vintage/LETRERO-VINTAGE-ANUNCIO-LUMINOSO-CAMALEON-VINTAGE-3-COLORES.webp"></div>
                      <div class="carousel-item"><img loading="lazy" class="w-100 img-fluid" data-target="#carouselExample" data-slide-to="9" alt="LETRERO-VINTAGE-ANUNCIO-LUMINOSO-LIQUOR-LAB-VINTAGE-LETRAS-ACRILICO-VERDE-BLANCO-2 - Alu-Mex" title="LETRERO-VINTAGE-ANUNCIO-LUMINOSO-LIQUOR-LAB-VINTAGE-LETRAS-ACRILICO-VERDE-BLANCO-2 - Alu-Mex"  src="/img/Letreros_Vintage/LETRERO-VINTAGE-ANUNCIO-LUMINOSO-LIQUOR-LAB-VINTAGE-LETRAS-ACRILICO-VERDE-BLANCO-2.webp"></div>
                      <div class="carousel-item"><img loading="lazy" class="w-100 img-fluid" data-target="#carouselExample" data-slide-to="10" alt="LETRERO-VINTAGE-ANUNCIO-LUMINOSO-LIQUOR-LAB-VINTAGE-LETRAS-ACRILICO-VERDE-BLANCO-3 - Alu-Mex" title="LETRERO-VINTAGE-ANUNCIO-LUMINOSO-LIQUOR-LAB-VINTAGE-LETRAS-ACRILICO-VERDE-BLANCO-3 - Alu-Mex"  src="/img/Letreros_Vintage/LETRERO-VINTAGE-ANUNCIO-LUMINOSO-LIQUOR-LAB-VINTAGE-LETRAS-ACRILICO-VERDE-BLANCO-3.webp"></div>
                      <div class="carousel-item"><img loading="lazy" class="w-100 img-fluid" data-target="#carouselExample" data-slide-to="11" alt="LETRERO-VINTAGE-ANUNCIO-LUMINOSO-LIQUOR-LAB-VINTAGE-LETRAS-ACRILICO-VERDE-BLANCO - Alu-Mex" title="LETRERO-VINTAGE-ANUNCIO-LUMINOSO-LIQUOR-LAB-VINTAGE-LETRAS-ACRILICO-VERDE-BLANCO - Alu-Mex"  src="/img/Letreros_Vintage/LETRERO-VINTAGE-ANUNCIO-LUMINOSO-LIQUOR-LAB-VINTAGE-LETRAS-ACRILICO-VERDE-BLANCO.webp"></div>
                      <div class="carousel-item"><img loading="lazy" class="w-100 img-fluid" data-target="#carouselExample" data-slide-to="12" alt="LETRERO-VINTAGE-BRASSI-ANUNCIO-LUMINOSO-3D-VINTAGE - Alu-Mex" title="LETRERO-VINTAGE-BRASSI-ANUNCIO-LUMINOSO-3D-VINTAGE - Alu-Mex"  src="/img/Letreros_Vintage/LETRERO-VINTAGE-BRASSI-ANUNCIO-LUMINOSO-3D-VINTAGE.webp"></div>
                      <div class="carousel-item"><img loading="lazy" class="w-100 img-fluid" data-target="#carouselExample" data-slide-to="13" alt="LETRERO-VINTAGE-CAFE-ANUNCIO-LUMINOSO-VINTAGE-3D - Alu-Mex" title="LETRERO-VINTAGE-CAFE-ANUNCIO-LUMINOSO-VINTAGE-3D - Alu-Mex"  src="/img/Letreros_Vintage/LETRERO-VINTAGE-CAFE-ANUNCIO-LUMINOSO-VINTAGE-3D.webp"></div>
                      <div class="carousel-item"><img loading="lazy" class="w-100 img-fluid" data-target="#carouselExample" data-slide-to="14" alt="LETRERO-VINTAGE-CAMALEON-2-ANUNCIO-LUMINOSO-VINTAGE-3D - Alu-Mex" title="LETRERO-VINTAGE-CAMALEON-2-ANUNCIO-LUMINOSO-VINTAGE-3D - Alu-Mex"  src="/img/Letreros_Vintage/LETRERO-VINTAGE-CAMALEON-2-ANUNCIO-LUMINOSO-VINTAGE-3D.webp"></div>
                      <div class="carousel-item"><img loading="lazy" class="w-100 img-fluid" data-target="#carouselExample" data-slide-to="15" alt="LETRERO-VINTAGE-CAMALEON-ANUNCIO-LUMINOSO-VINTAGE-3D - Alu-Mex" title="LETRERO-VINTAGE-CAMALEON-ANUNCIO-LUMINOSO-VINTAGE-3D - Alu-Mex"  src="/img/Letreros_Vintage/LETRERO-VINTAGE-CAMALEON-ANUNCIO-LUMINOSO-VINTAGE-3D.webp"></div>
                      <div class="carousel-item"><img loading="lazy" class="w-100 img-fluid" data-target="#carouselExample" data-slide-to="16" alt="LETRERO-VINTAGE-CAMALEON-LUMINOSO-LETRERO-VINTAGE - Alu-Mex" title="LETRERO-VINTAGE-CAMALEON-LUMINOSO-LETRERO-VINTAGE - Alu-Mex"  src="/img/Letreros_Vintage/LETRERO-VINTAGE-CAMALEON-LUMINOSO-LETRERO-VINTAGE.webp"></div>
                      <div class="carousel-item"><img loading="lazy" class="w-100 img-fluid" data-target="#carouselExample" data-slide-to="17" alt="LETRERO-VINTAGE-CAMALEON-VINTAGE-2-FLECHA-INSTALACION - Alu-Mex" title="LETRERO-VINTAGE-CAMALEON-VINTAGE-2-FLECHA-INSTALACION - Alu-Mex"  src="/img/Letreros_Vintage/LETRERO-VINTAGE-CAMALEON-VINTAGE-2-FLECHA-INSTALACION.webp"></div>
                      <div class="carousel-item"><img loading="lazy" class="w-100 img-fluid" data-target="#carouselExample" data-slide-to="18" alt="LETRERO-VINTAGE-CAMALEON-VINTAGE-3-FLECHA - Alu-Mex" title="LETRERO-VINTAGE-CAMALEON-VINTAGE-3-FLECHA - Alu-Mex"  src="/img/Letreros_Vintage/LETRERO-VINTAGE-CAMALEON-VINTAGE-3-FLECHA.webp"></div>
                      <div class="carousel-item"><img loading="lazy" class="w-100 img-fluid" data-target="#carouselExample" data-slide-to="19" alt="LETRERO-VINTAGE-CAMALEON-VINTAGE-4-FLECHA - Alu-Mex" title="LETRERO-VINTAGE-CAMALEON-VINTAGE-4-FLECHA - Alu-Mex"  src="/img/Letreros_Vintage/LETRERO-VINTAGE-CAMALEON-VINTAGE-4-FLECHA.webp"></div>
                      <div class="carousel-item"><img loading="lazy" class="w-100 img-fluid" data-target="#carouselExample" data-slide-to="20" alt="LETRERO-VINTAGE-CATAMARAN-ANUNCIO-LUMINOSO-VINTAGE-3D - Alu-Mex" title="LETRERO-VINTAGE-CATAMARAN-ANUNCIO-LUMINOSO-VINTAGE-3D - Alu-Mex"  src="/img/Letreros_Vintage/LETRERO-VINTAGE-CATAMARAN-ANUNCIO-LUMINOSO-VINTAGE-3D.webp"></div>
                      <div class="carousel-item"><img loading="lazy" class="w-100 img-fluid" data-target="#carouselExample" data-slide-to="21" alt="LETRERO-VINTAGE-CNK-POST-ALUMEX-ACRILICO-2 - Alu-Mex" title="LETRERO-VINTAGE-CNK-POST-ALUMEX-ACRILICO-2 - Alu-Mex"  src="/img/Letreros_Vintage/LETRERO-VINTAGE-CNK-POST-ALUMEX-ACRILICO-2.webp"></div>
                      <div class="carousel-item"><img loading="lazy" class="w-100 img-fluid" data-target="#carouselExample" data-slide-to="22" alt="LETRERO-VINTAGE-CNK-POST-ALUMEX-ACRILICO-3 - Alu-Mex" title="LETRERO-VINTAGE-CNK-POST-ALUMEX-ACRILICO-3 - Alu-Mex"  src="/img/Letreros_Vintage/LETRERO-VINTAGE-CNK-POST-ALUMEX-ACRILICO-3.webp"></div>
                      <div class="carousel-item"><img loading="lazy" class="w-100 img-fluid" data-target="#carouselExample" data-slide-to="23" alt="LETRERO-VINTAGE-COHIBA-HOTEL-LUMINOSO-ANUNCIO-3D-LUMINOSO - Alu-Mex" title="LETRERO-VINTAGE-COHIBA-HOTEL-LUMINOSO-ANUNCIO-3D-LUMINOSO - Alu-Mex"  src="/img/Letreros_Vintage/LETRERO-VINTAGE-COHIBA-HOTEL-LUMINOSO-ANUNCIO-3D-LUMINOSO.webp"></div>
                      <div class="carousel-item"><img loading="lazy" class="w-100 img-fluid" data-target="#carouselExample" data-slide-to="24" alt="LETRERO-VINTAGE-DEBARBAS-VINTAGE-FLECHA-3D-LETRERO-LUMINOSO - Alu-Mex" title="LETRERO-VINTAGE-DEBARBAS-VINTAGE-FLECHA-3D-LETRERO-LUMINOSO - Alu-Mex"  src="/img/Letreros_Vintage/LETRERO-VINTAGE-DEBARBAS-VINTAGE-FLECHA-3D-LETRERO-LUMINOSO.webp"></div>
                      <div class="carousel-item"><img loading="lazy" class="w-100 img-fluid" data-target="#carouselExample" data-slide-to="25" alt="LETRERO-VINTAGE-GRAN-CIBELES-ANUNCIO-LUMINOSO-VINTAGE-3D - Alu-Mex" title="LETRERO-VINTAGE-GRAN-CIBELES-ANUNCIO-LUMINOSO-VINTAGE-3D - Alu-Mex"  src="/img/Letreros_Vintage/LETRERO-VINTAGE-GRAN-CIBELES-ANUNCIO-LUMINOSO-VINTAGE-3D.webp"></div>
                      <div class="carousel-item"><img loading="lazy" class="w-100 img-fluid" data-target="#carouselExample" data-slide-to="26" alt="LETRERO-VINTAGE-LA-COLOMBA-NEGRO-ROJO-ACRILICO-TERMOFORMADO-2 - Alu-Mex" title="LETRERO-VINTAGE-LA-COLOMBA-NEGRO-ROJO-ACRILICO-TERMOFORMADO-2 - Alu-Mex"  src="/img/Letreros_Vintage/LETRERO-VINTAGE-LA-COLOMBA-NEGRO-ROJO-ACRILICO-TERMOFORMADO-2.webp"></div>
                      <div class="carousel-item"><img loading="lazy" class="w-100 img-fluid" data-target="#carouselExample" data-slide-to="27" alt="LETRERO-VINTAGE-LETS-HIT-THE-ROAD-3D-ANUNCIO-LUMINOSO-VINTAGE - Alu-Mex" title="LETRERO-VINTAGE-LETS-HIT-THE-ROAD-3D-ANUNCIO-LUMINOSO-VINTAGE - Alu-Mex"  src="/img/Letreros_Vintage/LETRERO-VINTAGE-LETS-HIT-THE-ROAD-3D-ANUNCIO-LUMINOSO-VINTAGE.webp"></div>
                      <div class="carousel-item"><img loading="lazy" class="w-100 img-fluid" data-target="#carouselExample" data-slide-to="28" alt="LETRERO-VINTAGE-MEXSI-ROCU-ANUNCIO-LUMINOSO-VINTAGE-3D - Alu-Mex" title="LETRERO-VINTAGE-MEXSI-ROCU-ANUNCIO-LUMINOSO-VINTAGE-3D - Alu-Mex"  src="/img/Letreros_Vintage/LETRERO-VINTAGE-MEXSI-ROCU-ANUNCIO-LUMINOSO-VINTAGE-3D.webp"></div>
                      <div class="carousel-item"><img loading="lazy" class="w-100 img-fluid" data-target="#carouselExample" data-slide-to="29" alt="LETRERO-VINTAGE-OSHKOSH-3D-ANUNCIO-LUMINOSO-VINTAGE - Alu-Mex" title="LETRERO-VINTAGE-OSHKOSH-3D-ANUNCIO-LUMINOSO-VINTAGE - Alu-Mex"  src="/img/Letreros_Vintage/LETRERO-VINTAGE-OSHKOSH-3D-ANUNCIO-LUMINOSO-VINTAGE.webp"></div>
                      <div class="carousel-item"><img loading="lazy" class="w-100 img-fluid" data-target="#carouselExample" data-slide-to="30" alt="LETRERO-VINTAGE-SUSHI-ANUNCIO-LUMINOSO-VINTAGE-3D - Alu-Mex" title="LETRERO-VINTAGE-SUSHI-ANUNCIO-LUMINOSO-VINTAGE-3D - Alu-Mex"  src="/img/Letreros_Vintage/LETRERO-VINTAGE-SUSHI-ANUNCIO-LUMINOSO-VINTAGE-3D.webp"></div>
                      <div class="carousel-item"><img loading="lazy" class="w-100 img-fluid" data-target="#carouselExample" data-slide-to="31" alt="LETRERO-VINTAGE-TEIKIT-ANUNCIO-LUMINOSO-VINTAGE-3D - Alu-Mex" title="LETRERO-VINTAGE-TEIKIT-ANUNCIO-LUMINOSO-VINTAGE-3D - Alu-Mex"  src="/img/Letreros_Vintage/LETRERO-VINTAGE-TEIKIT-ANUNCIO-LUMINOSO-VINTAGE-3D.webp"></div>
                      <div class="carousel-item"><img loading="lazy" class="w-100 img-fluid" data-target="#carouselExample" data-slide-to="32" alt="LETREROS-LUMINOSOS-DON-GALLO-ROSTICEROS-FACHADA-INSTALACION-ACERO-RAUTEADO-4 - Alu-Mex" title="LETREROS-LUMINOSOS-DON-GALLO-ROSTICEROS-FACHADA-INSTALACION-ACERO-RAUTEADO-4 - Alu-Mex"  src="/img/Letreros_Vintage/LETREROS-LUMINOSOS-DON-GALLO-ROSTICEROS-FACHADA-INSTALACION-ACERO-RAUTEADO-4.webp"></div>
                      <div class="carousel-item"><img loading="lazy" class="w-100 img-fluid" data-target="#carouselExample" data-slide-to="33" alt="LETREROS-VINTAGE-DON-GALLO-ROSTICEROS-FACHADA-INSTALACION-ACERO-RAUTEADO-5 - Alu-Mex" title="LETREROS-VINTAGE-DON-GALLO-ROSTICEROS-FACHADA-INSTALACION-ACERO-RAUTEADO-5 - Alu-Mex"  src="/img/Letreros_Vintage/LETREROS-VINTAGE-DON-GALLO-ROSTICEROS-FACHADA-INSTALACION-ACERO-RAUTEADO-5.webp"></div>
                      
                    </div>
                    <a class="carousel-control-prev" href="#carouselExample" role="button" data-slide="prev">
                      <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                      <span class="sr-only">Previous</span>
                    </a>
                    <a class="carousel-control-next" href="#carouselExample" role="button" data-slide="next">
                      <span class="carousel-control-next-icon" aria-hidden="true"></span>
                      <span class="sr-only">Next</span>
                    </a>
                  </div>
                </div>
                <div class="modal-footer">
                  <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
              </div>
            </div>
          </div>

          <!-- CONTACTO -->
          <div class="col-md-12">
            <h2 class="parpadea text pt-5 ">PARA ASESORIA Y COTIZACIONES DA CLICK AQUI</h2>
            <div class="w-100 mb-5 mt-3">
              <a href="tel:5552080808">
               <img class="w-100" src="https://www.anunciosluminososdemexico.com/assets/images/boton-contacto-para-anuncios-luminosos-totems-y-unipolares-cajas-de-luz-3d-letreros-de-acrilico-letras-de-acero-inoxidable.png" alt="">
              </a>
            </div>
          </div>
          <!-- CONTACTO -->

          <div class="row">
            <div class="col-md-7 text-justify">
              <p>Los anuncios luminosos o letreros luminosos de alumex son materiales de comunicación visual grafica volumetricos, generalmente iluminados con algun sistema electroluminicente para la exposicion de una o varias imágenes. Los anuncios luminosos o letreros luminosos de Alumex son colocados en el  punto de venta o canal de distribucion de alguna marca comercial, los anuncios luminosos o letreros luminosos se utiliza con el fin de imponer la visibilidad de alguna institucion, empresa, marca comercial o simplemente algun anuncio publicitario con el fin de llegar a un publico masivo en el mercado nacional o internacional.</p>
              <p>Los anuncios luminosos o letreros luminosos de Alumex, crean diferentes modelos y tipos de morfologias ya que los anuncios luminosos en alumex son hechos y fabricados con diferentes formas y materiales que se ajustan a la morfologia, tamaño ubicación geografica o simplemente las necesidades solicitadas por cada cliente especifico.</p>
            </div>
            <div class="col-md-5">
              <img loading="lazy" src="/img/Anuncios_PYMEs/LETRERO-LUMINOSO-VINTAGE-ALUMEX-BRASSI.webp" alt="Letrero Luminoso SAMSUNG - ALUMEX" title="Letrero Luminoso SAMSUNG - ALUMEX" class="img-fluid"/>
            </div>
          </div>
          <div class="row">
            <div class="col-md-4 text-justify">
              <p><img loading="lazy" src="/img/Anuncios_Corporativo/LETRERO-LUMINOSO-COCACOLA-VINTAJE-LETRAS-FOCOS.webp" alt="Letrero Luminoso Market - ALUMEX" title="Letrero Luminoso Market - ALUMEX"  class="img-fluid"/></p>
              <p><img loading="lazy" src="/img/banner_index/08_ANUNCIO-LUMINOSO-CAMALEON-VINTAGE.webp" alt="Letrero Intercam - Alumex" title="Letrero Intercam - Alumex"  class="img-fluid"/></p>
            </div>
            <div class="col-md-8 text-justify">
              <p>Por lo general todos nuestros anuncios luminosos o letreros luminosos,  los fabricamos con materiales de la mas alta calidad, seleccionando siempre productos con materias primas que a largo plazo se puedan reciclar y sean amigables con el medio ambiente como: acero inoxidable, acero al carbon, acero galvanizado, acero negro, aluminio natural, aluminio anodizado, placas en varios calibres, acrilicos, policarbonatos, vidrio, plasticos en varios calibres y colores, poliestirenos en varios acabados, poliespumas, peliculas vinilicas, lonas de alta flexibilidad y duracion en varios calibres. hay que contemplar que tambien en algunos casos especificos como proyectos especiales llegamos a utilizar materiales diferentes como: fibra de carbono, fibra de vidrio, maderas, piedras tipo onix y marmoles, yesos y grabados, resinas y metales varios.</p>
  
              <p><strong>Procesos de fabricacion de anuncios luminosos o letreros luminosos.</strong></p>
  
              <p>1.- Para crear o fabricar cualquier anuncio luminoso o letrero luminoso, como primer paso solicitamos o creamos un diseño para la planificacion del anuncio luminoso, junto con (si es el caso) la creacion de una memoria de calculo o memoria descriptiva.</p>
  
              <p>2.- Creacion de plantillas y trazos de los anuncios luminosos o letreros luminosos, para cortes de materiales primarios</p>
  
              <p>3.- Cortes generales de materiales primarios para los anuncios luminosos</p>
  
              <p>4.- Fabricacion general de estructura para los anuncsios luminosos según el caso</p>
  
              <p>5.- Ensambles, pegados o atornillado de bases con contornos, para proseguir con los acabados de los anuncios luminosos o letreros luminosos en pintura automotiva con bicapa o bien electrostatica según el caso en todos sus contornos.  </p>
  
              <p>6.- Cableado interno y conexión de sistemas de iluminacion completo  de los anuncios luminosos o letreros luminosos.</p>
  
              <p>7.- Abrillantado para refraccion y potencia de iluminacion del anuncio luminoso o letreros luminoso</p>
  
              <p>8.- Colocacion en el anuncio luminoso o letrero luminoso de pelicula o material de proyeccion frontal como acrilico o lona de alta densidad.</p>
  
              <p>9.- Colocacion en el anuncio luminoso o letrero luminoso de pelicula plastica de color a eleccion para proteccion y proyeccion de logotipo o mensaje deseado.</p>
  
              <p>10.- colocacion en el anuncio luminoso de molduras interiores y exteriores con el fin de obtener mayor estabilidad y durabilidad en todos los casos de fabricación.</p>
  
              <p>11.- transportación y montaje del anuncio luminoso o letrero luminoso en sitio a nivel nacional por parte de nuestro equipo de instaladores.</p>
            </div>
          </div>
          <div class="row">
            <div class="col-md-12">
                <h4 class="page-header">Por que somos la mejor opción del mercado  en fabricación de anuncios luminosos?</h4>
            </div>
          </div>
          <div class="row">
            <div class="col-md-6 text-justify">
              <p>como empresa de fabricacion de anuncios luminosos somos la mejor opcion ya que somos el tamaño ideal de empresa que nuestros clientes necesitan, somos una empresa mediana con vision de mantener siempre un modelo de negocio compacto con el fin de ahorrar costos que muchas veces el cliente termina pagando en otras empresas, contamos con la calidad y los precios acordes al mercado de acuerdo al producto final entregado y estamos  comprometidos con la satisfaccion total de nuestros clientes. los anuncios luminosos de alumex son la mejor opcion ya que cuentan con garantia de 7 a 10 años en exterior, e incluso de 15 años en interior, aclarando que es dependiendo el uso y mantenimiento preventivo que se implemente año con año para cada anuncio luminoso según el caso.</p>
            </div>
            <div class="col-md-6 text-justify">
              <p>los anuncios luminosos de alumex son supervisados por nuestros tecnicos especialistas en este ramo con antigüedad de mas de 30 años en el medio de este tipo de fabricacion, lo que nos hace una de las empresas mas capacitada en nuestra categoria de productos y servicios.</p>
              <p>los anuncios luminosos de alumex estan iluminados con sistemas de leds de alta potencia y de bajo calentamiento y consumo de energia electrica, contribuyendo asi a la disminucion del efecto invernadero provocado por el calentamiento del planeta al consumir mas energia de la necesaria y bajando los costos directos en su utilizacion.</p>
            </div>
          </div>
          <div class="row">
            <div class="col-md-12 mb-3">
              <h3><p class="text-center"><strong>Anuncios luminosos de ALUMEX es la mejor opcion por servicio, calidad, garantia, sustentabilidad y metodologia de fabricación.</strong></p></h3>
            </div>
          </div>
          <!--Off/Contenido-->
        </div>
      </div>
      <!--Body_content/Off-->
    <!--Footer/On-->
      <?php 
            require_once('./components/footer.php');
        ?>
      <!--Footer/Off-->
    </main>
    <?php 
        require_once('./components/navfloat.php');
    ?>
  </body>
</html>