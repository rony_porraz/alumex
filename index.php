<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="es">
<head>
    <?php 
        require_once('./components/config.php');
    ?>
    <title>Alu-mex - Fabricamos Anuncios Luminosos, Letreros Luminosos y Totems Espectaculares Para Todo México</title>
    <link rel="stylesheet" href="style/boot.css">
    <link type="text/css" rel="stylesheet" media="all" href="/js/led_banero/jquerysctipttop.css" />
    <link type="text/css" rel="stylesheet" media="all" href="./style/style_base.css" />
    <link rel="preconnect" href="https://maxcdn.bootstrapcdn.com/" crossorigin="" />
    <link rel="preconnect" href="https://www.youtube.com/" crossorigin="" />
    <link rel="preconnect" href="https://www.anunciosluminososdemexico.com/" crossorigin="" />
    <link rel="dns-prefetch" href="https://maxcdn.bootstrapcdn.com/" />
    <link rel="dns-prefetch" href="https://www.youtube.com/" />
    <link rel="dns-prefetch" href="https://www.anunciosluminososdemexico.com/" />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css" />
    <script type="text/javascript" src="js/jquery.js" ></script>
    <script type="text/javascript" src="js/popper.js" defer></script>
    <script type="text/javascript" src="js/boot.js" defer ></script>
    <script type="text/javascript" src="/js/led_banero/jquery.leddisplay.js"  ></script>
</head>

<body>
    <main role="main" class="container">
        
        <!--Head_Baner/On-->
        <div class="container dv_head"> <img src="/img/logo_alu.png" alt="Anuncios Luminosos - Alumex"
                title="Anuncios Luminosos - Alumex" class="img-fluid logo_des" /> <img src="/img/img_bagheader.png"
                alt="Anuncios Luminosos - Alumex" title="Anuncios Luminosos - Alumex" class="img-fluid ban_head " />
        </div>
        <!--Head_Baner/Off-->
        <!--Menu/On-->
        <?php 
            require_once('./components/menu.php');
        ?>
        <!--Menu/Off-->
        
        <!--Body_content/On-->
        <div class="container ">
            <div class="col-md-12 conte_base ">
                <div class="row ">
                    
                <!-- COL IZQUIERDA -->
                    <div class="col-md-6">
                        
                        <ul class="list-info">
                            <li><a href="anuncios-luminosos.php">Anuncios Luminosos</a></li>
                            <li><a href="letreros-luminosos.php">Letreros Corporativos</a></li>
                            <li><a href="totems-y-unipolares.php">Tótems, Fabricación de Espectaculares</a></li>
                            <li><a href="letreros-vintage.php">Letreros Vintage</a></li>
                        </ul>

                        <div class="w-100 text-center">
                            
                            <div class="img-thumbnail " style="width: 100%;">
                                
                                <div>
                                
                                    <h5 style="text-transform: uppercase;"> <strong>Visita nuestras redes sociales</strong> </h5>
                                
                                </div>
                                
                                <div class="text-center" style="display: flex; flex-direction: row; justify-content: space-around; font-size: xx-large; padding: 10px; align-items: center;">
                                    
                                    <div class="col-3">
                                        <a
                                            href="https://www.facebook.com/anunciosluminososalumex/?ti=as"
                                            target="_blank">
                                        <img
                                            width="75%"
                                            src="/img/f_logo_RGB-Blue_58.webp"
                                            alt="Icono Facebook - Anuncios Luminosos Alumex"
                                            title="Icono Facebook - Anuncios Luminosos Alumex"> 
                                        </a> 
                                    </div>

                                    <div class="col-3"> 
                                        <a  href="https://api.whatsapp.com/send?phone=525519964610&text=Hola%20gracias%20por%20visitar%20ALUMEX.%20Para%20poder%20saber%20m%c3%a1s%20de%20tu%20proyecto%20y%20apoyarte%20de%20manera%20m%c3%a1s%20profesional.%20Envianos%20imagenes%20o%20planos%20de%20la%20idea%20general%20que%20tienes%20y%20as%c3%ad%20ser%c3%a1%20m%c3%a1s%20f%c3%a1cil%20y%20r%c3%a1pido%20distinguir%20tu%20necesidad%20a%20nuestro%20servicio.%20Gracias%20por%20elegir%20ALUMEX%20REFRESH%20BRANDS%20MEXICO.%20Si%20tienes%20cualquier%20duda%20marcanos%20al%20%2052%2855%2952080808%20ext.%201"
                                            target="_blank">
                                        <img
                                            width="75%"
                                            src="/img/WhatsApp_Logo.webp"
                                            alt="Icono Whatsapp - Anuncios Luminosos Alumex"
                                            title="Icono Whatsapp - Anuncios Luminosos Alumex"> 
                                        </a>
                                    </div>
                                    <div class="col-3">
                                        <a href="https://g.page/AlumexAnunciosLuminosos?gm" target="_blank"> 
                                            <img
                                                width="75%"
                                                src="/img/google.webp"
                                                alt="Icono Google - Anuncios Luminosos Alumex"
                                                title="Icono Google - Anuncios Luminosos Alumex"
                                            /> 
                                        </a> 
                                    </div>
                                    <div class="col-3">
                                        <a href="https://www.youtube.com/playlist?list=PLJnEQSV6HwUJqrF5j7yu2Snv395i22Xal" target="_blank">  
                                            <img
                                                width="75%"
                                                src="https://anunciosluminososdemexico.com/assets/images/socialmedia/canal%20youtube%20anuncios%20alumex%20mexico%20canal%20anuncios%20luminosos%20para%20todo%20mexico%20anuncios%203d.png"
                                                alt="Icono Canal YouTube Alumex - Anuncios Luminosos Alumex"
                                                title="Icono Google - Anuncios Luminosos Alumex"
                                            /> 
                                        </a> 
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="ocultarcuadro">
                        
                        <h1 class="titulon-h1 text-center">
                            FABRICANTES DE ANUNCIOS Y LETREROS CORPORATIVOS 
                        </h1>
                        <div id="carouselControlsindex3" class="carousel slide" data-ride="carousel"
                        style="margin:0px 0px 0px 0px;">
                        <div class="carousel-inner" role="listbox" id="carousel1">
                                <?php 
                                    require('./components/carousel/car1.php');
                                ?>
                        </div>
                        <a
                            class="carousel-control-prev" 
                            href="#carouselControlsindex3" 
                            role="button"
                            data-slide="prev"
                        >
                        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                        <span class="sr-only">Previous</span>
                        </a>
                        <a class="carousel-control-next" href="#carouselControlsindex3" role="button" data-slide="next">
                            <span class="carousel-control-next-icon" aria-hidden="true"></span>
                            <span class="sr-only">Next</span>
                        </a>
                    </div>

                    <div>
                            <!--/-->
                            <div class="jquery-script-ads">
                                <script
                                    defer
                                    type="text/javascript"> google_ad_client = "ca-pub-2783044520727903"; google_ad_slot = "2780937993"; google_ad_width = 728; google_ad_height = 0; </script>
                            </div>
                            <div style="margin: 5px 0px"> <canvas class="canvasld" style=" width: 100%; height: auto;">
                                    Fabricamos toda clase de letreros luminosos, anuncios luminosos, espectaculares y
                                    Totems , somos una empresa respaldada por mas de 200 marcas comerciales que han
                                    confiado en nuestra calidad y buen servicio, contamos con talleres especializados
                                    con mas de 30 años de respaldo y experiencia , en la fabricación de tu letrero
                                    luminoso, anuncios , o tótem , te asesoramos desde la apertura de un local comercial
                                    , hasta la colocación de un espectacular para tu centro de comercio, hotel,
                                    restaurante, cafetería, plaza comercial, construimos fachadas, anuncios luminosos,
                                    letreros corporativos, unipolares , tótems, locales comerciales , fabricamos muebles
                                    y displays de exhibición, P.O.P. a la medida de tu proyecto y promocion. Fabricamos
                                    todas las ideas de nuestros clientes , vamos a toda la republica, nos comprometemos
                                    a la medida del compromiso de nuestros clientes, 52080808 con 5 líneas, Contamos con
                                    asesores 8 hrs al día para cada una de tus dudas o desarrollos necesarios
                                    Colaboramos con todo tipo de empresas, Exportamos anuncios a cualquier parte del
                                    mundo. La calidad no esta peleada con precios justo dentro de mercado , somos
                                    fabricantes de ideas y colaboramos con la industria del comercio, nos encantan los
                                    proyectos de cualquier tipo y estamos apasionados con construir y fortalecer marcas
                                    , checa nuestro curriculum y galerías para ver que te conviene mas, contamos con
                                    showroom para presentarte muestras físicas y presentación empresarial o bien nuestra
                                    planta de producción para visitas de proyectos en elaboración y producción. En el
                                    2005 colaboramos con la fabricación y colocación de toda la señalización del
                                    aeropuerto Benito Juarez de la Ciudad de México. Hemos fabricado letras de 4 mts de
                                    alto para resisitencias de vientos superiores a 120 kms x hr , espectaculares con
                                    alturas de 42 mts con peso de 30 toneladas, Hemos colaborado con empresas de la
                                    talla de Pepsico, Walmart, Loreal, Samsung, Nextel, Teran Tbwa, Gamesa, Game Planet,
                                    Sanborns, Sears.</canvas>
                                <script defer
                                    type="text/javascript"> var options = { pixelSize: 15, stepDelay: 20, horizontalPixelsCount: 100, verticalPixelsCount: 5, pixelRatio: 0.7 }; $('.canvasld, .crl').leddisplay($.extend(options, { pixelSize: 7.333 })); </script>
                            </div>
                            <!--/-->
                            <iframe
                            class="w-100 framebase dnoneresp"
                            src="https://www.youtube.com/embed/videoseries86e2.html?list=PLJnEQSV6HwUJqrF5j7yu2Snv395i22Xal"
                            frameborder="0"
                            allowfullscreen=""
                            loading="lazy">
                            </iframe> 
                            
                        </div>

                        </div>
                        
                        <div class="quitarmovil">
                                 <h1 class="titulon-h1 text-center">
                                FABRICACIÓN DE ANUNCIOS LUMINOSOS PARA PYMES
                        </h1>

                        <div id="carouselControlsindex4" class="carousel slide" data-ride="carousel">
                            <div class="carousel-inner carouselControlsindex4" role="listbox" id="carousel4">
                            <?php 
                                require('./components/carousel/car2.php');
                            ?>

                        </div>
                        
                        <a
                            class="carousel-control-prev" 
                            href="#carouselControlsindex4" 
                            role="button"
                            data-slide="prev"
                        >
                            <span
                                class="carousel-control-prev-icon"
                                aria-hidden="true">
                            </span>
                            <span class="sr-only">Previous</span>
                        </a> 
                        <a
                            class="carousel-control-next"
                            href="#carouselControlsindex4" 
                            role="button"
                            data-slide="next"
                        >
                        <span
                            class="carousel-control-next-icon"
                            aria-hidden="true">
                        </span>
                        <span class="sr-only">Next</span></a>
                    </div>

                    <iframe
                                class="w-100 mt-2 framebase"
                                src="https://www.youtube.com/embed/videoseries86e2.html?list=PLJnEQSV6HwUJqrF5j7yu2Snv395i22Xal"
                                frameborder="0"
                                allowfullscreen=""
                                style="margin-bottom: 10px;"
                                loading="lazy">
                        </iframe>
                        <div class="jquery-script-ads">
                            <script
                                defer
                                type="text/javascript"> google_ad_client = "ca-pub-2783044520727903"; google_ad_slot = "2780937993"; google_ad_width = 728; google_ad_height = 0; </script>
                        </div>

                        

                        <div style="margin: 5px 0px">
                            
                            <canvas class="canvasld" style=" width: 100%; height: auto;">
                                En ALUMEX Contamos con un gran compromiso social, apoyando desde 2002 a empresas PYMES en su crecimiento y desarrollo, teniendo ya casi 30 casos de éxito, expansión y crecimiento con apoyo de nuestras ideas y propuestas de adecuación e imagen comercial exterior e interior para la fabricación de anuncios luminosos y letreros luminosos.
                            </canvas>
                            <script defer type="text/javascript">
                                var options = { pixelSize: 15, stepDelay: 20, horizontalPixelsCount: 100, verticalPixelsCount: 5, pixelRatio: 0.7 }; $('.canvasld, .crl').leddisplay($.extend(options, { pixelSize: 7.333 })); 
                            </script>
                        </div>
                        </div>
                       
                    

                    <div class="carouselControlsindex2">
                        <!--/-->

                        <h2 class="titulon-h1" style="font-size: 23px;margin-top: 10px; text-transform: uppercase;text-align: left;"> Fabricamos  te y asesoramos para tu compra de
                            anuncios luminosos y letreros luminosos en todo México </h2>
                        <ul class="list-link">
                            <li><a href="beneficios.php#precuaciones">PRECAUCIÓN! COMO ASEGURAR MI INVERSION</a></li>
                            <li><a href="beneficios.php#beneficios">BENEFICIOS ALUMEX</a></li>
                            <li><a href="beneficios.php#objetivo">OBJETIVO PRINCIPAL ALUMEX</a></li>
                            <li><a href="beneficios.php#importancia">IMPORTANCIA PARA ALUMEX</a></li>
                            <li><a href="beneficios.php#garantia">GARANTÍAS ALUMEX</a></li>
                            <li><a href="beneficios.php#retos">RETOS ALUMEX</a> </li>
                            <li><a href="beneficios.php#mision">MISIÓN ALUMEX</a></li>
                            <li><a href="beneficios.php#vision">VISIÓN ALUMEX</a></li>
                            <li><a href="mas.html">MAS...</a></li>
                        </ul> 

                        <!--/-->
                    </div>

                        
                    </div>
                    <!-- COL IZQUIERDA END -->
                    
                    <!-- COL DERECHA -->
                    <div class="col-md-6">
                    
                        <div class="quitarmovil">
                            

                        <h1 class="titulon-h1 text-center"> FABRICANTES DE ANUNCIOS Y LETREROS CORPORATIVOS </h1>



                            <div id="carouselControlsindex" class="carousel slide" data-ride="carousel"
                                style="margin:0px 0px 0px 0px;">
                                <div class="carousel-inner" role="listbox" id="carousel1">
                                        <?php 
                                            require('./components/carousel/car1.php');
                                        ?>
                                </div>
                                <a
                                    class="carousel-control-prev" 
                                    href="#carouselControlsindex" 
                                    role="button"
                                    data-slide="prev"
                                >
                                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                <span class="sr-only">Previous</span>
                                </a>
                                <a class="carousel-control-next" href="#carouselControlsindex" role="button" data-slide="next">
                                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                    <span class="sr-only">Next</span>
                                </a>
                            </div>

                            <div>
                                    <!--/-->
                                    <div class="jquery-script-ads">
                                        <script
                                            defer
                                            type="text/javascript"> google_ad_client = "ca-pub-2783044520727903"; google_ad_slot = "2780937993"; google_ad_width = 728; google_ad_height = 0; </script>
                                    </div>
                                    <div style="margin: 5px 0px"> <canvas class="canvasld" style=" width: 100%; height: auto;">
                                            Fabricamos toda clase de letreros luminosos, anuncios luminosos, espectaculares y
                                            Totems , somos una empresa respaldada por mas de 200 marcas comerciales que han
                                            confiado en nuestra calidad y buen servicio, contamos con talleres especializados
                                            con mas de 30 años de respaldo y experiencia , en la fabricación de tu letrero
                                            luminoso, anuncios , o tótem , te asesoramos desde la apertura de un local comercial
                                            , hasta la colocación de un espectacular para tu centro de comercio, hotel,
                                            restaurante, cafetería, plaza comercial, construimos fachadas, anuncios luminosos,
                                            letreros corporativos, unipolares , tótems, locales comerciales , fabricamos muebles
                                            y displays de exhibición, P.O.P. a la medida de tu proyecto y promocion. Fabricamos
                                            todas las ideas de nuestros clientes , vamos a toda la republica, nos comprometemos
                                            a la medida del compromiso de nuestros clientes, 52080808 con 5 líneas, Contamos con
                                            asesores 8 hrs al día para cada una de tus dudas o desarrollos necesarios
                                            Colaboramos con todo tipo de empresas, Exportamos anuncios a cualquier parte del
                                            mundo. La calidad no esta peleada con precios justo dentro de mercado , somos
                                            fabricantes de ideas y colaboramos con la industria del comercio, nos encantan los
                                            proyectos de cualquier tipo y estamos apasionados con construir y fortalecer marcas
                                            , checa nuestro curriculum y galerías para ver que te conviene mas, contamos con
                                            showroom para presentarte muestras físicas y presentación empresarial o bien nuestra
                                            planta de producción para visitas de proyectos en elaboración y producción. En el
                                            2005 colaboramos con la fabricación y colocación de toda la señalización del
                                            aeropuerto Benito Juarez de la Ciudad de México. Hemos fabricado letras de 4 mts de
                                            alto para resisitencias de vientos superiores a 120 kms x hr , espectaculares con
                                            alturas de 42 mts con peso de 30 toneladas, Hemos colaborado con empresas de la
                                            talla de Pepsico, Walmart, Loreal, Samsung, Nextel, Teran Tbwa, Gamesa, Game Planet,
                                            Sanborns, Sears.</canvas>
                                        <script defer
                                            type="text/javascript"> var options = { pixelSize: 15, stepDelay: 20, horizontalPixelsCount: 100, verticalPixelsCount: 5, pixelRatio: 0.7 }; $('.canvasld, .crl').leddisplay($.extend(options, { pixelSize: 7.333 })); </script>
                                    </div>
                                    <!--/-->
                                    <iframe
                                    class="w-100 framebase dnoneresp"
                                    src="https://www.youtube.com/embed/videoseries86e2.html?list=PLJnEQSV6HwUJqrF5j7yu2Snv395i22Xal"
                                    frameborder="0"
                                    allowfullscreen=""
                                    loading="lazy">
                                    </iframe> 
                                    
                                </div>
                        </div>

                        <div class="ocultarcuadro">
                                 <h1 class="titulon-h1 text-center">
                                FABRICACIÓN DE ANUNCIOS LUMINOSOS PARA PYMES
                        </h1>

                        
                        <div id="carouselControlsindex2" class="carousel slide" data-ride="carousel">
                            <div class="carousel-inner carouselControlsindex2" role="listbox" id="carousel2">
                            <?php 
                                require('./components/carousel/car2.php');
                            ?>

                        </div>
                        
                        <a
                            class="carousel-control-prev" 
                            href="#carouselControlsindex2" 
                            role="button"
                            data-slide="prev"
                        >
                            <span
                                class="carousel-control-prev-icon"
                                aria-hidden="true">
                            </span>
                            <span class="sr-only">Previous</span>
                        </a> 
                        <a
                            class="carousel-control-next"
                            href="#carouselControlsindex2" 
                            role="button"
                            data-slide="next"
                        >
                        <span
                            class="carousel-control-next-icon"
                            aria-hidden="true">
                        </span>
                        <span class="sr-only">Next</span></a>
                    </div>

                    <div class="jquery-script-ads">
                        <script
                                defer
                                type="text/javascript"> google_ad_client = "ca-pub-2783044520727903"; google_ad_slot = "2780937993"; google_ad_width = 728; google_ad_height = 0; </script>
                            </div>
                            
                            
                            
                            <div style="margin: 5px 0px">
                                
                                <canvas class="canvasld" style=" width: 100%; height: auto;">
                                    En ALUMEX Contamos con un gran compromiso social, apoyando desde 2002 a empresas PYMES en su crecimiento y desarrollo, teniendo ya casi 30 casos de éxito, expansión y crecimiento con apoyo de nuestras ideas y propuestas de adecuación e imagen comercial exterior e interior para la fabricación de anuncios luminosos y letreros luminosos.
                                </canvas>
                                <script defer type="text/javascript">
                                var options = { pixelSize: 15, stepDelay: 20, horizontalPixelsCount: 100, verticalPixelsCount: 5, pixelRatio: 0.7 }; $('.canvasld, .crl').leddisplay($.extend(options, { pixelSize: 7.333 })); 
                                </script>
                        </div>
                        <iframe
                                    class="w-100 mt-2 framebase"
                                    src="https://www.youtube.com/embed/videoseries86e2.html?list=PLJnEQSV6HwUJqrF5j7yu2Snv395i22Xal"
                                    frameborder="0"
                                    allowfullscreen=""
                                    style="margin-bottom: 10px;"
                                    loading="lazy">
                            </iframe>
                        </div>

                        <div class="w-100">
                            <!--/On-->
                            <div class="col-md-12">
                                <h3 style="text-transform: uppercase;font-weight: 700;" class="parpadea text titulon-h1">
                                    Comprometidos con las empresas de Mexico fabricando anuncios luminosos con 100% calidad.
                                </h3>
                                <div class="mb-3">
                                    <div>
                                        <div class="w-100"> 
                                            <a href="tel:5552080808">
                                                <img
                                                    class="w-100"
                                                    src="./img/boton-contacto-para-anuncios-luminosos-totems-y-unipolares-cajas-de-luz-3d-letreros-de-acrilico-letras-de-acero-inoxidable.png" 
                                                    alt="Aviso De Privacidad - Alu-Mex"
                                                >
                                            </a>
                                            <label class="form-check-label w-100" for="ck_acepta">
                                                <a href="aviso.php" class="w-100 d-block" style="text-align: center;text-transform: uppercase;" >Ver aviso privacidad </a>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                        
                                <div class="w-100 col-md-12 text-center mb-3">
                                    
                                    <div class="img-thumbnail">
                                        <a href="#" data-toggle="modal" data-target="#myModal5">
                                            <div>
                                                <h1 class="titulon-h1" style="text-transform: uppercase;"> Vacantes y proveedores para anuncios luminosos y letreros luminosos alumex </h1>
                                            </div>
                                            <img
                                                src="/img/bolsa de trabajo en alumex anuncios luminosos 3d cajas de luz luminosas totems cdmx.png"
                                                alt="Bolsa de trabajo - Anuncios Luminosos Alumex"
                                                title="Bolsa de trabajo - Anuncios Luminosos Alumex"
                                                style="max-width: 400px; width: 90%;" 
                                            />
                                        </a> 
                                    </div>
                                
                                    <iframe
                                        class="w-100 mt-2"
                                        loading="lazy"
                                        src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3762.2808274108875!2d-99.15953363640537!3d19.443455368065095!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x85d1f8cf85f59927%3A0x62e32ff9d52611e4!2sAnuncios%20Luminosos%20Alumex%20y%20Letreros%20Luminosos%20Alumex!5e0!3m2!1ses-419!2smx!4v1600796934695!5m2!1ses-419!2smx"
                                        width="600"
                                        height="255"
                                        frameborder="0"
                                        style="border:0;"
                                        allowfullscreen=""
                                        aria-hidden="false"
                                        tabindex="0">
                                    </iframe>
                                    
                                </div>
    
                            </div>
                        </div>

                    </div>
                    <!-- COL DERECHA END -->

                </div>
            </div>
        </div>
        <!--Body_content/Off-->
        <div class="modal fade" id="myModal5" role="dialog">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header"> <button type="button" class="btn btn-default"
                            data-dismiss="modal">Cerrar</button>
                    </div>
                    <div> </div>
                    <div class="modal-body">
                        
                        <h6 class="mb-2 modal-title parpadea text">Bolsa De Trabajo</h6>

                        <form method="POST" action="contacto.php" >

                            <div class="mb-1">
                                <label for="exampleInputEmail1" class="form-label">Nombre</label>
                                <input type="text" class="form-control" name="nombre" aria-describedby="emailHelp" required>
                            </div>

                            <div class="mb-1">
                                <label for="exampleInputEmail1" class="form-label">Correo Electronico</label>
                                <input type="email" class="form-control" name="correo" aria-describedby="emailHelp" required>
                            </div>
                            <div class="mb-1">
                                <label for="exampleInputPassword1" class="form-label">Teléfono</label>
                                <input type="tel" name="tel" class="form-control" required>
                            </div>
                            <button type="submit" class="btn btn-dark mt-3 w-100">Enviar</button>
                        </form>
                    </div>
                    <div class="modal-footer"> </div>
                </div>
            </div>
        </div>
        <!--/-->
        <!-- Google Code for Conversiones 2015 Julio Conversion Page -->
        <script
            type="text/javascript"> /* <![CDATA[ */ var google_conversion_id = 967206652; var google_conversion_language = "en"; var google_conversion_format = "2"; var google_conversion_color = "ffffff"; var google_conversion_label = "Z7qHCJ2g014Q_M2ZzQM"; var google_remarketing_only = false; /* ]]> */ </script>
        <script type="text/javascript" src="./js/index.js"></script>
        <!--Footer/On-->
        <?php 
            require_once('./components/footer.php');
        ?>
        <!--Footer/Off-->
    </main>
    
    <?php 
        require_once('./components/navfloat.php');
    ?>

</body>
</html>