<div class="container">
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark bg-transárent">
        <button 
            class="navbar-toggler text-center"
            type="button"
            data-toggle="collapse"
            data-target="#navbarNavAltMarkup"
            aria-controls="navbarNavAltMarkup" 
            aria-expanded="false"
            aria-label="Toggle navigation"
            style="margin: 0 auto;"
        >
            <a href="tel:+525552080808">
                <img src="/img/logo_alu.png" alt="Anuncios Luminosos - Alumex" title="Anuncios Luminosos - Alumex" class="img-fluid" style="width:90%; margin-top:15px" />
            </a>
            <a href="tel:+525552080808">
                <p class="text-center" style="font-size:1rem;">
                    <span class="fa fa-phone"></span> Da click y
                    llamanos!
                </p>
            </a>
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
            <div class="navbar-nav justify-content-around" style="width:100%;">
                <a class="nav-item nav-link " href="/">
                    Inicio
                    <span class="sr-only">(current)</span>
                </a>
                <a class="nav-item nav-link" href="anuncios-luminosos.php">Anuncios Luminosos CORPORATIVOS</a>
                <a class="nav-item nav-link" href="letreros-luminosos.php">Letreros Luminosos PYMES</a>
                <a class="nav-item nav-link" href="totems-y-unipolares.php">Totems, Espectaculares y Unipolares</a>
                <a class="nav-item nav-link" href="letreros-vintage.php">Letreros Vintage</a>
                <a class="nav-item nav-link" href="contacto.php">Contacto</a>
            </div>
        </div>
    </nav>
</div>