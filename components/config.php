<?php
    $telefono = '5519964610';
    $correo_1 = 'ventas@alumex.com';
    $correo_2 = 'ventas@anunciosalumex.com';
    $subTitle = 'Fabricamos Anuncios Luminosos, Letreros Luminosos y Totems Espectaculares Para Todo México';
?>

<meta charset="utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<link type="text/css" rel="shortcut icon" href="/img/favicon.png" />
<link type="image/x-icon" rel="icon" href="/img/favicon.png" />
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<meta name="author" content="Luminosos y Letreros Nacionales SA. de CV." />
<meta name="Owner" content="ventas@anunciosalumex.com">
<meta name="Robots" content="INDEX,FOLLOW">
<meta name="description" content="letreros luminosos, marquesinas luminosas, anuncios espectaculares, anuncios unipolares, anuncios totems, totems luminosos, pilones, pylons, signs mexico, fabricantes de anuncios, fabricantes de unipolares, fabricantes de espectaculares, fabricantes de totems, fabricantes de letreros, fabricantes de anuncios luminosos, letreros corporativos, toldos retráctiles, toldos retráctiles luminosos, gabinetes luminosos, gabinetes de luz, gabinetes 3d, cajas de luz, cajas de luz 3d, caja de luz acrílico, caja de luz lona, caja de luz led, anuncios de leds, leds de colores, fachadas en leds, iluminación de fachadas" />
<meta name="Abstract" content="letreros luminosos, marquesinas luminosas, anuncios espectaculares, anuncios unipolares, anuncios totems, totems luminosos, pilones, pylons, signs mexico, fabricantes de anuncios">
<meta name="Keywords" content="anuncios luminosos, letreros luminosos, letreros de aluminio, letras 3d, anuncios de acrilico, letreros para empresas, cajas de luz, totems publicitarios, unipolares, fabricacion de espectaculares, anuncios para empresas, anuncios vintage, letreros vintage ">
<link rel="preconnect" href="https://maxcdn.bootstrapcdn.com/" crossorigin="" />
<link rel="preconnect" href="https://www.youtube.com/" crossorigin="" />
<link rel="preconnect" href="https://www.anunciosluminososdemexico.com/" crossorigin="" />
<link rel="dns-prefetch" href="https://maxcdn.bootstrapcdn.com/" />
<link rel="dns-prefetch" href="https://www.youtube.com/" />
<link rel="dns-prefetch" href="https://www.anunciosluminososdemexico.com/" />
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css" />

<script async src="https://www.googletagmanager.com/gtag/js?id=UA-165020589-1"></script>
<script> window.dataLayer = window.dataLayer || []; function gtag() { dataLayer.push(arguments); } gtag('js', new Date()); gtag('config', 'UA-165020589-1'); </script>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-178719413-1"></script>
<script> window.dataLayer = window.dataLayer || []; function gtag() { dataLayer.push(arguments); } gtag('js', new Date()); gtag('config', 'UA-178719413-1');</script>
<!-- END - Google Analytics -->