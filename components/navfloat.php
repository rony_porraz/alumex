<aside class="botonw">
        <div class="botonw__item">
            <a target="_blank" href="https://www.facebook.com/anunciosluminososalumex/?ti=as" class="item__container">
                <img loading="lazy" class="item__img" src="./img/iconos/facebook icono anuncios luminoso de mexico alumex.svg" alt="Icono Facebook Anuncios Luminosos Mexacril">
                <p class="item__name">Facebook</p>
            </a>
        </div>
        <div class="botonw__item">
            <a 
                target="_blank"
                class="item__container"
                href="https://api.whatsapp.com/send?phone=525519964610&text=Hola%20gracias%20por%20visitar%20ALUMEX.%20Para%20poder%20saber%20m%c3%a1s%20de%20tu%20proyecto%20y%20apoyarte%20de%20manera%20m%c3%a1s%20profesional.%20Envianos%20imagenes%20o%20planos%20de%20la%20idea%20general%20que%20tienes%20y%20as%c3%ad%20ser%c3%a1%20m%c3%a1s%20f%c3%a1cil%20y%20r%c3%a1pido%20distinguir%20tu%20necesidad%20a%20nuestro%20servicio.%20Gracias%20por%20elegir%20ALUMEX%20REFRESH%20BRANDS%20MEXICO.%20Si%20tienes%20cualquier%20duda%20marcanos%20al%20%2052%2855%2952080808%20ext.%201"
            >
                <img   loading="lazy" class="item__img" src="./img/iconos/whatsapp icono anuncios luminoso de mexico alumex.svg" alt="Icono WhatsApp Anuncios Luminosos Mexacril">
                <p class="item__name">WhatsApp</p>
            </a>
        </div>
        <div class="botonw__item">
            <a target="_blank" href="https://www.google.com/maps/place//data=!4m2!3m1!1s0x85d1f8cf85f59927:0x62e32ff9d52611e4?source=g.page.m" class="item__container">
                <img  loading="lazy" class="item__img" src="./img/iconos/google bussines icono anuncios luminoso de mexico alumex.svg" alt="Icono Google Business Anuncios Luminosos Mexacril">
                <p class="item__name">Business</p>
            </a>
        </div>
        <div class="botonw__item">
            <a href="tel:5552080808" class="item__container">
                <img  loading="lazy" class="item__img" src="./img/iconos/telefono icono anuncios alumex.svg" alt="Icono Facebook Anuncios Luminosos Mexacril">
                <p class="item__name">Teléfono</p>
            </a>
        </div>
        <div class="botonw__item">
            <a href="mailto:ventas@anunciosalumex.com?Subject=Email%20de%20contacto%20" class="item__container">
                <img  loading="lazy" class="item__img" src="./img/iconos/gmail icono anuncios luminoso de mexico alumex.svg" alt="Icono Facebook Anuncios Luminosos Mexacril">
                <p class="item__name">Email</p>
            </a>
        </div>
    
</aside>