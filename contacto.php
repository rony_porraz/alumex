<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="es">
  <head>
    <?php 
        require './components/config.php';
    ?>
    <title>Contacto - <?php echo $subTitle;?></title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <link type="text/css" rel="stylesheet" media="all" href="/style/style_base.css" />
    <link type="text/css" rel="stylesheet" media="all" href="/js/led_banero/jquerysctipttop.css" />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
    <script type="text/javascript" src="/js/led_banero/jquery.leddisplay.js"></script>
  </head>
  <body>
    <main role="main" class="container">
      <!--Head_Baner/On-->
      <div class="container dv_head">
        <img loading="lazy" src="/img/logo_alu.png" alt="Alumex" title="Alumex" class="img-fluid logo_des"/>
        <img loading="lazy" src="/img/img_bagheader.png" alt="Alumex" title="Alumex" class="img-fluid ban_head  "/>
      </div>
      <!--Head_Baner/Off-->
      <!--Menu/On-->
        <?php 
            require_once('./components/menu.php');
        ?>
      <!--Menu/Off-->
			<!--Body_content/On-->
			<div class="container">
				<div class="col-md-12 conte_base">
					<!--On/ form_Zoho-->
					<div class="col-md-12">
						<h4 class="parpadea text pb-1 pt-4">PARA ASESORIA Y COTIZACIONES</h4>
						<h4 class="parpadea text pb-4 pt-1">CHECA NUESTRO CONTACTO</h4>
					</div>
					<div class="row">
						<div class="col-md-12 text-center mb-3">
							<img loading="lazy" src="/img/showroom-1.webp" width="100%">
						</div>
					</div>
					<div class="col-md-12">
						<!--FormContact/On-->
						<div class="col-md-12">
							<div class="w-100 mb-5 mt-3">
							  <a href="tel:5552080808">
							   <img class="w-100" src="https://www.anunciosluminososdemexico.com/assets/images/boton-contacto-para-anuncios-luminosos-totems-y-unipolares-cajas-de-luz-3d-letreros-de-acrilico-letras-de-acero-inoxidable.png" alt="">
							  </a>
							</div>
						  </div>
					</div>
					<div class="row">
						<div class="col-md-12 text-center">
							<img loading="lazy" class="img-fluid" src="/img/shipping-2.webp">
						</div>
					</div>
					<!--FormContact/Off-->        
					<!--Off/ form_Zoho-->
					<div class="row">
						<div class="col-md-12">
							<div class="text-center">
								<p class="text-center">Llamanos en <strong><h3>Ciudad de Mexico al <a href="tel:+5552080808">+52 (55) 52.08.08.08</a></h3></strong> </p>
								<p>o bien escribenos a</p> 
								<p><h4><strong>ventas@anunciosalumex.com</strong></h4></p>
								<p><h4><strong>ventas@anunciosalumex.com</strong></h4></p>
								
							<p class="text-center"><h4>Vamos a toda la republica y latinoamerica ¡!! </h4></p>
							</div>
							<div class="text-justify">
								<p>Nuestra sede se encuentra en Reforma Centro en la Ciudad de Mexico on planta en Edo de Mexico Ecatepec y vamos a toda la republica y latinoamerica con el fin de cubrir mejor nuestra area de accion y proyeccion comercial.</p>
							</div>
						</div>
					</div>         
				</div>
			</div>
			<!--Body_content/Off-->

    <!--Footer/On-->
        <?php 
            require_once('./components/footer.php');
        ?>
    <!--Footer/Off-->
    
    </main>

    <?php 
        require_once('./components/navfloat.php');
    ?>
  
</body>
</html>