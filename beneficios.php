<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="es">

<head>
  <?php
  require_once('./components/config.php');
  ?>
  <title> Benefocios Alu-Mex - Anuncios Luminosos, Letreros Luminosos y Totems Espectaculares Para Todo México</title>
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
  <link type="text/css" rel="stylesheet" media="all" href="/style/style_base.css" />
  <link type="text/css" rel="stylesheet" media="all" href="/style/gallery.css" />
  <link type="text/css" rel="stylesheet" media="all" href="/js/led_banero/jquerysctipttop.css" />
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
  <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
  <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
  <script type="text/javascript" src="/js/led_banero/jquery.leddisplay.js"></script>
</head>

<body>

  <main role="main" class="container">

    <!--Head_Baner/On-->
    <div class="container dv_head"> <img src="/img/logo_alu.png" alt="Anuncios Luminosos - Alumex" title="Anuncios Luminosos - Alumex" class="img-fluid logo_des" /> <img src="/img/img_bagheader.png" alt="Anuncios Luminosos - Alumex" title="Anuncios Luminosos - Alumex" class="img-fluid ban_head " />
    </div>
    <!--Head_Baner/Off-->
    <!--Menu/On-->
    <?php
    require_once('./components/menu.php');
    ?>
    <!--Menu/Off-->

    <!--Body_content/On-->
    <div class="container">
      <div class="col-md-12 conte_base">
        <div class="col-md-12 text-justify">

          <p></p>
          <p></p>

          <div class="col-md-12" name="beneficios" id="beneficios">
            <p><strong>BENEFICIOS ALUMEX:</strong></p>
          </div>
          <hr />
          <div class="col-md-9 text-justify">
            <p>La calidad y excelencia es un beneficio que pocos valoran tanto como ALUMEX. Por ello, los anuncios luminosos son ensamblados con materiales de primera, con altos estándares de calidad y normas internacionales de producción. En ALUMEX contamos con un sistema de selección minucioso de proveedores, lo que permite adquirir materiales con los más altos estándares con el objetivo de entregar a nuestros clientes un acabado con LA MAXIMA calidad e imagen internacional para sus empresas.</p>
          </div>

          <div class="col-md-3">
            <p><img src="img/logo_alu.png" alt="logo" title="logo" width="300" class="img-responsive" /></p>
          </div>

          <div class="col-md-6 text-justify">
            <p>Otro beneficio es nuestro sistema de colaboracion calendarizando cada uno de los requerimiento s de proyectos anuales de cada uno de nuestros clientes.</p>
            <p>Contamos con un sistema de sondeo de oportunidades y desarrollo conjunto con cada cliente lo que nos permite crear relaciones y lazos de confianza y apoyo constante a cada proyecto que se nos encomiende. Escuchamos y analizamos cada una de las ideas de los clientes y los apoyamos para desarrollarlas y mejorarlas con la labor de nuestro equipo de expertos con experiencia de más de tres decadas. Nuestro equipo esta siempre en capacitacion constante y especializado en cada area de la empresa por lo que con gusto le apoyamos en las necesidades ya que para nuestra empresa siempre habra nuevos retos que vencer.</p>
          </div>

          <div class="col-md-6">
          </div>


          <div class="col-md-12" name="mision" id="mision">
            <p><strong>MISION ALUMEX</strong></p>
          </div>
          <hr />

          <div class="col-md-6 text-justify">
            <p>Nuestra misión es contribuir al máximo en la consolidación de la imagen comercial de las más importantes marcas nacionales e internacionales y asistirlas en sus planes de expansión, aplicando siempre los más altos estándares en el diseño, fabricación y montaje de anuncios luminosos , totems, unipolares, carteleras, letreros corporativos y construcción comercial de interior o exterior.
          </div>
          <div class="col-md-3">
            <img src="/img/001.png" alt="Mision Para Todo Mexico Anunios Luminosos - Alumex" title="alu" width="300" class="img-responsive" />

          </div>
          <div class="col-md-12 text-justify"> Queremos ser la mejor opción para cumplir los planes de crecimiento de los más exigentes clientes del mercado nacional e internacional.</p>
          </div>
        </div>

        <div class="col-md-12">
          <div class="col-md-4">
          </div>

          <div class="col-md-8" name="vision" id="vision">
            <p><strong>VISION ALUMEX</strong></p>
          </div>
          <hr />
          <p class="text-justify">Estar siempre en la cima de la innovación, soluciones, productos y servicios en materia de anuncios luminosos e infraestructura comercial. Ser reconocidos por la calidad humana y profesional de nuestra gente y por nuestra contribución a la expansión de las mejores marcas.</p>
        </div>

        <div class="col-md-12">
          <div class="col-md-8" name="objetivo" id="objetivo">
            <p><strong>OBJETIVO PRINCIPAL ALUMEX</strong></p>
          </div>


          <div class="col-md-8 text-justify">
            <p>Mantener e incrementar el prestigio ganado durante más de 10 años en el mercado de los anuncios luminosos y competir con los mejores productos y servicios en un mercado global para llegar a otros países, iniciando por Latinoamérica. </p>
          </div>
          <div class="col-md-4">
          </div>

        </div>
        <div class="col-md-3">
          <div class="col-md-12">
          </div>
        </div>
        <div class="col-md-9">
          <div class="col-md-12" name="importancia" id="importancia">
            <p><strong>IMPORTANCIAS PARA ALUMEX</strong></p>
          </div>
          <p class="text-justify">Es de suma importancia para ALUMEX ofrecer diseños y desarrollos comerciales con detalles y acabados que reflejen la imagen corporativa que las grandes firmas requieren y desean como presentación. Con ello, ALUMEX contribuye y facilita los objetivos de crecimiento y expansión de nuestros clientes.</p>
        </div>

        <div class="col-md-3">
        </div>
        <div class="col-md-9 text-justify">
          <p>En ALUMEX no perdemos de vista la impecable imagen visual y el impacto que deben tener todos los nuestros productos, pues sabemos que con ello contribuimos a conquistar los retos de nuestros clientes y los de nosotros mismos.</p>

          <p>Nuestro equipo sabe perfectamente que su trabajo siempre debe contemplar la más alta virtud de los siguientes puntos estratégicos: desarrollo, diseño, calidad, impacto de imagen comercial y monto de inversión en mercado real competitivo.</p>


          <div class="col-md-12" name="retos" id="retos">
            <p><strong>RETOS ALUMEX</strong></p>
          </div>

          <p class="text-justify">Uno de los principales retos que tenemos como empresa de imagen exterior e interior es ofrecer una mejora constante de propuestas que agreguen valor a las marcas de nuestros clientes.</p>

          <p class="text-justify">Mantenernos siempre a la vanguardia y en capacitación constante nos permite enfrentar el reto de crear proyectos espectaculares que nos identifique siempre con los mejores.</p>
        </div>


        <div class="col-md-12 text-center" name="garantia" id="garantia">
          <hr />
          <p><strong>CONTRATO DE GARANTIAS DE FABRICACION DE ANUNCIOS LUMINOSOS ALUMEX</strong></p>
        </div>

        <div class="col-md-4">
        </div>
        <div class="col-md-4 text-justify">
          <p>Con el fin de ofrecer un mejor servicio a todos nuestros clientes, a continuación les presentamos nuestras políticas de garantías internas externas o por defectos de fabricación.</p>
        </div>
        <div class="col-md-4">
        </div>

        <div class="col-md-12 text-justify">
          <p class="text-justify"><strong> TERMINOS:</strong></p>
          <p class="text-justify"> 1.- ALUMEX garantiza el producto entregado por el tiempo de 6 meses a partir de la fecha de entrega o el pedido de dicho producto.</p>
          <p class="text-justify"> 2.- ALUMEX cumple con la responsabilidad de reponer total o parcialmente la elaboración o buen funcionamiento del producto solicitado , si este es reclamado en un lapso de 5 días naturales por defecto de fabricación o daño cualesquiera que fuera en el parámetro racional y objetivo de que el producto no cumpla con las características objeto de la función central del producto.</p>

          <p class="text-justify"> 3.- ALUMEX garantiza el armazón y cuerpo o estructura del producto entregado, por el tiempo de 5 años , pero en partes graficas como impresiones , acrílicos viniles , vidrios o recubrimientos, dependerán completamente del trato y uso así como de mantenimientos, preventivos y correctivos, que se le proporcione al producto en los intervalos en los que se indica por la empresa ALUMEX.</p>

          <p class="text-justify"> 4.- ALUMEX no garantiza, ni se hace responsable de ningún deterioro del producto entregado en los siguientes casos de daños estructurales:</p>

          <p class="text-justify">A) Que haya sido dañado por un tercero y se compruebe que mantiene daños ajenos a la empresa Alumex.</p>
          <p class="text-justify">B) Que el producto entregado presente fallas en sujecion por movimientos teluricos, malos climas , vientos excesivos, o desprendimientos por daños estructurales en zona donde se indico la colocacion del mismo.</p>
          <p class="text-justify">B) Que el producto por cuestiones de inclemencias de tiempos presente daños tanto electricos como estructurales.</p>

          <p class="text-justify">5.-ALUMEX no garantiza daños en partes eléctricas internas ni externas, ya que en partes eléctricas , los proveedores de productos de este tipo , no brindan garantías extendidas. En el caso de presentar fallas o anomalías , se brindara una garantia por tiempo limitado de 4 semanas , siempre y cuando se compruebe que se a hecho uso debido de los equipos y conexiones electricas y voltajes naturaleza de USO en cada proyecto entregado por la empresa ALUMEX.</p>

          <p class="text-justify">6.-ALUMEX respaldara con calidad el trabajo entregado desde el momento de aceptación por parte de cada uno de nuestros clientes, por lo que al momento de las entregas realizadas por política solicitamos firma de recepcion para la acreditacion de dicha garantía sin el documento de recepción de cada proyecto no se hará valida ninguna garantia correspondiente al proyecto solicitado.</p>
          <p class="text-justify">7.- En caso de reclamación de nuestras garantías , se tendrá que verificar visual, fisica y estructuralmente por parte de uno de nuestros tecnicos y en su caso por alguno de nuestros peritos en materia estructural, ya que nuestro trabajo y servicio depende completamente de el buen estado y funcionamiento del sitio señalado y autorizado por el cliente, por lo que previo a cualquier montaje o instalación de ALUMEX , es prioridad y responsabilidad del cliente verificar que las instalaciones cumplan con las condiciones de seguridad y estabilidad que necesite para el objeto de cubrir nuestro trabajo y para la entrega final de cada proyecto , ya que estas condiciones están autorizadas y previstas por el personal a cargo de nuestro cliente.
          </p>

          <p class="text-justify">Con el fin de entregar siempre productos de calidad le reiteramos nuestro mejor empeño y colaboración por lo que recomendamos revisar cada uno de nuestros productos para que a largo plazo pueda explotar lo mejor posible la vida útil de cada uno de nuestros productos.</p>

          <p class="text-center">Atentamente</p>
          <p class="text-center"><strong>DIRECCION GENERAL</strong></p>
          <p class="text-center"><strong>JOSE ANTONIO AGUILAR ROSALES</strong></p>
          <hr />
        </div>

        <div class="col-md-12">
          <div name="precuaciones" id="precuaciones">
            <p><strong>PRECAUCIONES ¡!! COMO ASEGURAR MI INVERSION:</strong></p>
          </div>

          <div class="col-md-4">
          </div>
          <div class="col-lg-7 text-justify">
            <p>1.- ES MUY SIMPLE PRIMERO ASEGURATE DE NO ESTAR TRATANDO CON UNA EMPRESA QUE NO TE OFRECE LOS MATERIALES QUE CUMPLAN CON LAS NORMAS Y LAS GARANTIAS NECESARIAS PARA LA DURABILIDAD DE LA IMAGEN COMERCIAL DE EL PROYECTO A DESARROLLAR.</p>

            <p>ES COMUN QUE NOS LLAMEN CLIENTES PIDIENDO EL APOYO PARA SOLUCIONAR LO QUE ALGUNOS PEQUEÑOS NEGOCIOS O EN SU CASO EMPRESAS NO PUDIERON SOLUCIONAR HASTA LA ENTREGA FINAL.</p>

            <p>PARA LLEGAR A ELLO TENEMOS QUE PLANEAR MUY BIEN DESDE EL PRINCIPIO EL PROYECTO DESEADO ENFOCANDONOS EN LOS DETALLES CRUCIALES QUE RECOMENDAMOS Y A SU VEZ NOS SOLICITA CADA CLIENTE , CREAMOS COMO CONCECUENCIA UNA ATMOSFERA RADIAL DE BUENOS RESULTADOS Y RECUPERACION DE LA INVERSION TOTAL.</p>

            <p>2.- EN ALGUNAS OCACIONES NOS SUCEDE QUE NOS LLAMAN POR QUE ALGUNAS SUPUESTAS EMPRESAS LES ENTREGARON UN PRODUCTO DE PESIMA CALIDAD O BIEN HASTA EN ALGUNOS CASOS NO LO ENTREGARON , POR ENDE ES DE SUMA IMPORTANCIA TRATAR CON EMPRESAS QUE TENGAN LA EXPERIENCIA COMPROBABLE E INCLUSO MUESTREN LAS RECOMENDACIONES NECESARIAS , CON EL FIN DE SABER QUE SE ESTA INVIRTIENDO EN UN DESARROLLO QUE OFRECERA LA MAYOR CALIDAD , DURABILIDAD E IMPACTO VISUAL POSIBLE.</p>

            <p>3.- EN CADA PROYECTO HAY ASEGURARSE DE AUTORIZAR Y ESTAR CONCIENTE DE CADA PUNTO RESUELTO Y EXPUESTO EN EL PRESUPUESTO FINAL , PARA NO LLEGAR A TENER CONFUSIONES FUTURAS EN EL MOMENTO DE ENTREGA FINAL POR PARTE DEL PROVEEDOR.</p>
          </div>

        </div>

      </div>



    </div>
    </div>

    <!--Body_content/Off-->

    <?php
    require_once('./components/footer.php');
    ?>

  </main>
  <?php
  require_once('./components/navfloat.php');
  ?>
</body>

</html>